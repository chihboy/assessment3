## Week 9 (20/05/2020)

* Assessment four released 

  * Spatial data science: Data input, manipulation, analysis and presentation

  * Worth 50% 

    * Group mark 45%
    * Individual reflection 5%

  * Three tasks

    * Exploratory analysis (data summaries, tables, charts and maps)
    * Data processing (extracting data, spatial joins, output to a GeoPackage)
    * Spatial analysis (Provide meaningful insights in to the data)

  * Structure

    * Modules + Jupyter notebook
    * Group work with git

    

### Going back to git

Since the beginning of the class we have used git to distribute and share class data, and provide versioning for our code in assessment 3. Cloning, pulling, comitting, and pushing are all fundamental tasks to git, however the ultimate purpose of git is working in teams. This involves merging data, dividing your workflow into feature branches and submitting code for review by your team.

In the [week 5 lab](./week5.md), we explored concepts of git, and in the recording we also saw what happens if two versions collide and you end up with a merge conflict. This will most likely happen more frequently if your team member(s) are working on the same part of the code together. You can continue the same way as you would, by resolving these merge conflicts manually, or you can try methods that will distance yourself away from them as much as possible, and allow for review.

**Branches**

You can create new branches in git, that allow you to work on a part of the project, separately from the project itself, then merge those changes to the **master** branch. In a git repository, the **master** branch is the where all the final work in progress should be held. You should only commit to the master in a way that doesn't break the program. Ideally, another person will review your changes through a pull request (github)/merge request (gitlab) - These are the same thing, but are called differently on different git management frontends (gitlab/github/bitbucket/etc).

You can read further about branches [here on Atlassian's guide](https://www.atlassian.com/git/tutorials/using-branches).

Say we have two group members. One has decided to work on the functions for printing a summary in task 1, and another member for creating tables. for At some point in the project, they will be working on the same file and might change something within the same function. There should ideally be **three** branches:

* **master** (which no one should be working directly on)
* A branch for the summary (let's just call it **summaryoutput**)
* Another for the table (let's call it **tables**)

Group member one can divert from master to summaryoutput with:

```bash
git checkout -b summaryoutput
```

Likewise for group member #2

```bash
git checkout -b tables
```

We use **-b** to create a new branch, but the **checkout** command allows us to move between branches, so if you want to move back to master or another already created branch, for example, just use

```bash
git checkout master
```

---

We now have two branches, to check which one you're in, type 

```bash
git branch
```

You will see a list of branches available to your local git repository, and an asterisk next to the one you're in.

```bash
  master
  summaryoutput
* tables

```

We have now frozen what was in the **master** branch at the time of the branch creation and can work on our new feature without interfering with anyone else's work (unless they are of course, using our branch as well).

Here you will make all of your changes, and then push them to your gitlab repository. 

In this example, I'll make a new file **tables.py** in folder **group999**, in the **tables** branch, push and then merge to master.

```bash
[evan@salamanca 999]$ mkdir group999           
[evan@salamanca 999]$ cd group999 
[evan@salamanca group999]$ touch tables.py
[evan@salamanca group999]$ cd ..      
[evan@salamanca 999]$ ls 
group999  README.md
[evan@salamanca 999]$ git add group999/
[evan@salamanca 999]$ git status       
On branch tables
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   group999/tables.py

[evan@salamanca 999]$ git push -u origin master
[evan@salamanca 999]$ git commit -m "Add tables module"
[tables 6a247d7] Add tables module
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 group999/tables.py
[evan@salamanca 999]$ git push                         
fatal: The current branch tables has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin tables

[evan@salamanca 999]$ git push --set-upstream origin tables

Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 16 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 317 bytes | 317.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for tables, visit:
remote:   https://gitlab.unimelb.edu.au/geom90042/a4/999/-/merge_requests/new?merge_request%5Bsource_branch%5D=tables
remote: 
To gitlab.unimelb.edu.au:geom90042/a4/999.git
 * [new branch]      tables -> tables
Branch 'tables' set up to track remote branch 'tables' from 'origin'.
[evan@salamanca 999]$ 

```



If we refresh out gitlab page, we can see two branches in the pull down menu

![image-20200520084902640](assets/image-20200520084902640.png)

We also don't see our new folder **group999** as it is in the new branch. If we select the tables branch we will see the contents of it.

![image-20200520084938740](assets/image-20200520084938740.png)

Let's create a new "Merge request" by selecting it on the left side of the page.

![image-20200520085052148](assets/image-20200520085052148.png)

From here we can select our **source branch** and **target branch**. The branch **tables** will be our source, that we want to merge into **master**.

![image-20200520085211306](assets/image-20200520085211306.png)

We can see the changes to all the files if we were to merge everything. We can also add a comment, and a title for the other group member to see.

![image-20200520085231443](assets/image-20200520085231443.png)

We can then submit, and then it is up to another person in the group to go over the work, approve it, or send it back. They can make comments on it and decide to reject it if they wish. If it's approved, Gitlab will merge all the changes into the master repository.



Remember to check which branch you're working on!

### Nbconvert to PDF

Like in A3, you will need to compile your Jupyter Notebook into latex to then be converted into PDF.

In the /resources/reproducible_reports/template folder there is a file called **latex_nocode.tplx**. Make sure this is included in your repository. Also, make sure you have **jupyter_contrib_nbextensions** also included in your environment with jupyter.

To avoid a lot of problems with the PDF compiler, install the version of pdflatex suitable for your operating system (instead of through Anaconda). You can get this from https://www.latex-project.org/get/

In your Jupyter Notebook, you will need to edit the metadata. Goto **Edit** -> **Edit Notebook Metadata**. You need to follow the instruction on what to do here, basically we're adding more information to our report (follow what is already in Martin's original notebook paper.ipnyb). For example this is what I had in the tutorial today:

```json
{
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
    "name": "python3",
    "display_name": "Python 3",
    "language": "python"
  },
  "language_info": {
    "name": "python",
    "version": "3.8.1",
    "mimetype": "text/x-python",
    "codemirror_mode": {
      "name": "ipython",
      "version": 3
    },
    "pygments_lexer": "ipython3",
    "nbconvert_exporter": "python",
    "file_extension": ".py"
  },
  "latex_metadata": {
    "affiliation": "The University of Melbourne",
    "author": "Evan",
    "bibliography": "references",
    "packages": "\\usepackage{algorithm}\\usepackage{algorithmic}",
    "title": "Report"
  }
}
```



Then, from the command line (within your Environment!):

```bash
jupyter nbconvert --exec --to latex --template latex_nocode.tplx --output myanalysis.tex myanalysis.ipynb
```

Make sure you take note of what's happening here! **latex_nocode.tplx** is wherever you put it (you will need to specify this), this removes all the code from your cells in your final output, **myanalysis.tex** is the file you want to output from this command and **myanalysis.ipynb** is your jupyter notebook filename.

Then after it is complete with no errors, compile it into a PDF!

```bash
pdflatex myanalysis.tex
```

Done. You should have your PDF **myanalysis.pdf**.

**Resources**

[Merge requests on gitlab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

[Using git with Jupyter](https://nextjournal.com/schmudde/how-to-version-control-jupyter)

[Git branches (Atlassian guide)](https://www.atlassian.com/git/tutorials/using-branches)

