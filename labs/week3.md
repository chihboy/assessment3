## Week 3

**Assessment two released**

* Introduction to Spatial Programming - Trajectory analysis
  * Using Python to formulate 
  
  * The project information sheet is available on Canvas, and,
  
  * Is also found, along with the corresponding data file and unit test file on [this repository](../assessments/a2).
  
    

**Syncing changes from the git repository**

To obtain updates from this repository we need to retrieve them with git, this can be done via two methods; ```git pull``` or ```git fetch```. The differences are minor but can make a big impact on your workflow if you are working on the same code with other people at the same time. 

* The command ```git pull``` will download all changes from the repository and attempt to merge them with any changes on the same files you have done. If there are any conflicts, it will ask you to edit the file before committing any changes.
* Alternatively, ```git fetch``` will download the changes, but store them all in a branch, separate to your workflow. This is the safest option, but means you need to merge the files in a second step. This is generally useful if you want to look at all the changes first, and decide what needs to be merged.

**In our case**, because we are not submitting code to the repository, and the local copy of the repository has likely not changed since the last time we cloned it, using ```git pull``` is the best option here. Make sure you're in the folder of the repository on your hard drive, and then issue the command either in Git bash (on Windows), or the terminal (on MacOS/Linux).



**Preparing our workflow**

First we should setup a folder and an environment to work in. For best practices, the [PROJECT STRUCTURE](../resources/PROJECT_STRUCTURE.md) resource outlines what is generally expected in a development workflow, however, the submission requirements for this project are only four individual files:

* assessment2.py
* assessment2.csv
* assessment2_out.txt
* environment.yml

So the structure of the project folder for assessment one is up to you but keep note, that the **test script**, and the i**nput csv datafile** is expected to be within the same folder as your submission script in your code, so leaving everything in the one folder is fine.



**Create an environment**

In the second week lab, we installed Anaconda and practised setting up an environment. If you weren't able to do this last week, follow the instructions in the [ENVIRONMENTS](../resources/ENVIRONMENTS.md) resource first. To create an environment for this project, in the **Anaconda Prompt** (Windows) or the **terminal** (MacOS/Linux), assuming you want to call your environment **a2** (it can be anything you would like), type:

```bash
conda create --name a2 python=3.7
```

After it has installed, enter the environment by issuing the command:

```
source activate a2
```

You will likely need to install one or two more packages into this environment for assessment 2 (for example, in Q 4.1.2 and Q 4.4.2). Make sure you always do this whilst you're in the environment.

To install a new library or python application, for example flake8 (in Q 4.4.2) type:

```
conda install flake8
```

If you want to use jupyter notebook in this environment, you will need to install it also into it.

```
conda install jupyter
```

This can then be run by typing into the command line (Anaconda Prompt/Terminal):

```
jupyter notebook
```



**Prepare files**

Copy the assessment files (trajectory_data.csv, test_assessment2.py) into the folder you created above and create your new Python file with a text editor/Python IDE of your choice.

Remember to look through the assessment sheet first to look at the requirements -- There are questions requiring the correct setup of a main() function, as well as the naming of other functions, and their input arguments, to be consistent.