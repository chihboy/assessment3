## Week 4 (01/04/2020)

**Notes**

* Assessment two's submission date changed
  * Due 12/04/2020 (23:59)
* Individual feedback for assessment one should be available on Canvas. General comments:
  * Latitude is the **Y** axis. Easy to confuse.
  * Projection transformations can introduce accuracy loss in data, however they are likely to keep the data relatively close to it's original location. 
    * If your re-projection is moving your data 100s of kilometres away, you're doing something wrong! Even a change in a few metres can be problematic.
    * QGIS/ESRI/modern GIS tools in general can display more than one projection at the same time. If you have a global map in WGS84, you will be able to visualise a map from GDA94 Zone 55 at the same time with no problems. Just be careful doing analysis on two data sets from different projection systems
  * Careful with assumptions! If your assumption that the data's scale is correct because it **looks** like it, **prove it**. Performing the latter is not so easy if you have unverifiable data. For example, if the state of Victoria (such as the one in assessment 1) has the same scale, but geographically located elsewhere, we can't assume it's the same size unless we check it against an official data set. We should not use it for geometry calculations.



**Assessment two**

* Anaconda environment setup
* Cloned the repository with git
* Read and written CSV data
* Transformed coordinates
* Written functions to perform analysis
* Anyone any further? Test cases?



**Testing code**

In assessment two we are provided with test cases to test our functions. Testing is really important in large projects where multiple people are working on many different elements of code. The idea is that you can test your functions for validity automatically by creating test cases. In this subject we will play around with unit tests, where we can test the validity of individual functions and see if their outputs are correct. On large scale developments, continuous integration pipelines are used to include many unit tests, so when code is committed to a repository, it is tested for bugs and warnings are issued if tests fail. 

In our assessment we are provided with [test_assessment2.py](../assessments/a2/test_assessment2.py). What is happening inside it?

---

```python
#!/usr/bin/python3
```



This "shebang" lets the computer know that this is able to be executed by Python. The /usr/bin/python3 statement is the location of the Python 3 binary file generally in a Unix (MacOS/Linux) system. It's common practise to include this a the beginning of a Python program.

---

```python
''' This script runs a set of unittests on the Python
    code named assessment2.py written for the class
    GEOM90042: Spatial Information Programming.

    Execute by entering your Anaconda environment and
    typing from the command line:

    python test_assessment2.py -v

    This will test a few functions and their outputs
    for their correctness. '''

```



This docstring allows the user to know a little bit about the program. Here we can see that the program can be run a certain way. Using the -v option in the command tells the program (and ultimately the **unittest** library) that we want the verbose mode. This provides us more information about our tests than the program usually would.

---

```python
import unittest
import os
import assessment2
```



Import statements. These should be familiar already. Here we are importing:

* unittest, an [inbuilt Python testing library](https://docs.python.org/3/library/unittest.html).
* os, another [inbuilt Python librar](https://docs.python.org/3/library/os.html)y to allow access to the <u>**o**</u>perating <u>**s**</u>ystem. As methods to access files, for example, are different between Windows, Mac, Linux. This library allows the same file access methods to run on any computer no matter the operating system. You can use this library to list files, delete files, access system information such as CPU core counts. You can see it's use in the setUp() function below.
* assessment2, this is the assesment file you are writing. Here we are importing all of the functions you have written into this test program, so we can access them like any other library. We don't need to put the .py extension here, as Python already knows this.

---

```python
class TestTaskOne(unittest.TestCase):
    def setUp(self):
        ''' Sets up a few variables to run test cases in Task 1'''
        self.filename = os.path.join(os.getcwd(), "trajectory_data.csv")
        self.test_coordinate = (4326, 4796, 116.318417, 39.984702)
        self.test_coordinate_answer = (441782.79591610597, 4428131.247163864)

    def test_import_csv(self):
        ''' Test the import_csv function '''
        csv_output = assessment2.import_csv(self.filename)
        self.assertTrue(isinstance(csv_output, (list, dict, tuple)),
                        "Output not a list, tuple or dictionary. \
                        Ignore this error if you defined a class.")

    def test_project_coordinate(self):
        ''' Tests the project_coordinate function '''
        projected_coordinate = assessment2.project_coordinate(
            *self.test_coordinate)
        self.assertTrue(isinstance(projected_coordinate, tuple),
                        "Projected coordinate not a tuple")
        self.assertEqual(len(projected_coordinate), 2,
                         "Function does not return exactly 2 coordinates")
        self.assertTrue(isinstance(projected_coordinate[0], float),
                        "Projected X coordinate not a float, seems unlikely")
        self.assertTrue(isinstance(projected_coordinate[1], float),
                        "Projected Y coordinate not a float, seems unlikely")

        # Test X and Y projection coordinates with verified proj4 projection
        self.assertAlmostEqual(assessment2.project_coordinate(
            *self.test_coordinate)[0], self.test_coordinate_answer[0])
        self.assertAlmostEqual(assessment2.project_coordinate(
            *self.test_coordinate)[1], self.test_coordinate_answer[1])

```



Here we have a class. When we import unittest, we can import classes from it which are objects written specifically for the library. In Python we can inherit classes by including the parent class in parenthesis such as above

```python
class TestTaskOne(unittest.TestCase):
```



This sets us up to write a unit test for TaskOne. We chose this name so we know when we test, it will help us logically identify which part of our code the functions are being tested in. Therefore, these functions are related to Task 1 of our assessment.

First we have to prepare some variables that will be used in our test functions. In unittest, we need to specify this function as setUp(self). Here we can create whatever variables we want, that we want to use in our tests. This needs to occur for every test class (TestTaskOne, TestTaskTwo, etc) we create.

We then create a function to test a specific feature we're created. This **needs** to start with test_. So we have:

```python
def test_import_csv(self):
```

and here, what we are doing is replicating the steps that would happen to test the function:

* Import a filename (that we setup in the setUp function beforehand) self.filename
* Check the type of the function's returned variable. Here is needs to be either; a list, a tuple or a dictionary.
* The AssertTrue function means the isinstance() function, which tests the type of our variable, returns a true, and if it doesn't, will tell us it failed in the command line when we run our tests.

There are many ways to test functions in unittest, we can see a few in [unittest.TestCase()](https://docs.python.org/3/library/unittest.html#unittest.TestCase).

---

We do the same for some more functions, and for a function in Task two, then finally we issue:

```python
if __name__ == '__main__':
    unittest.main()
```



We've seen this before. When we run our test script, we only want to call the unittest's main() method and exit. Here we are specifying this.



In assessment two, you are not required to write another test case, but this will come up again later in the class. If you want to practice writing one for assessment two in your submission (with no penalty or reward to your marks), I am happy to provide feedback.