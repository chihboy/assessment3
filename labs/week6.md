## Week 6 (29/04/2020)

* Assessment two results online
  * Example answer in the repository



**Common mistakes**

* No exception handling
  * Python errors and exceptions https://docs.python.org/3/tutorial/errors.html
  * Built in exception codes https://docs.python.org/3/library/exceptions.html#OSError.errno

* For loops and variables

  * Use:

  ```python
  for row in list:
      print(row)
  ```

  * Don't use

  ```python
  for i in range(len(list)):
  	print(list[i])
  ```

  * If you need an index enumerator:
  ```python
  for i, row in enumerate(list):
  	print("Index {}: ".format(i, row))
  ```

* Calculation of average speed
  * Not total speed over iterations
    * This is assuming velocity is constant and ignoring changes in acceleration. In real life this is almost never the case
    * Formula for calculating this should be **Average velocity = distance/time**
    * Refer to this link on calculating average velocity http://hyperphysics.phy-astr.gsu.edu/hbase/vel2.html

* Constants
  * Repeated in code
  * outside of main() or global scope

* Complexity of data structures

  * Lists vs dict vs class
  * List a lot simpler for this task. Dictionaries have benefits of speed, but become more complex when iterating through trajectories
  * Classes overkill for the task, good for larger projects

* Writing to file

  * **open()** has a method **write()**, this will "print" a line to your opened file:
  ```python
  with open(filename, "w") as file:
  	file.write("some text")
  ```



