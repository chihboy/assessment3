## Week 5 (21/04/2020)

**Notes**

* Assessment three released 
  * Working with Rasters and Digital Elevation Models
  * 20%
  * Due May 3 at 23:59



**Assessment two**

* Explores Digital Elevation Models (DEM) and processing them with Python
* Programmatically exporting scientific reports
* Building test cases
* Using git



**Git**

We have touched briefly on some basics with git so far. We have been able to clone a repository to our local machine and retrieve changes from it. In assessment 3 you will be required to use git in a repository of your own to track changes.

## Getting started

You should now have access to a repository in the gitlab, for example, as my student ID is **evant1**, my repository is located at https://gitlab.unimelb.edu.au/geom90042/student/evant1. You will be greeted with a repository page such as the one below instructing you on different methods to start using the repository.

We want to clone and start using this repository, just like we did for **geom90042**, to somewhere on our hard drive. We can follow the instructions in the below instruction page for **Create a new repository**. 

![image-20200421201340905](assets/image-20200421201340905.png)



From git bash (for Windows), or the terminal (for MacOS/Linux), go to a suitable directory that you would like to set up your repository. I will choose the folder I have in my home directory called SpatialInfo. This is where I'm keeping all the files for this class including the central repository.

```bash
[evan@gijon SpatialInfo]$ ls -l
total 16
drwxr-xr-x 8 evan users 4096 Apr 20 09:17 geom90042
drwxr-xr-x 5 evan users 4096 Apr  1 09:32 studentarea
drwxr-xr-x 8 evan users 4096 Apr 19 13:17 teaching_resources
drwxr-xr-x 3 evan users 4096 Apr  1 08:37 testarea
[evan@gijon SpatialInfo]$ 
```

From here I can clone my repository, using the links provided on the gitlab page by pressing the clone button. Because I am using SSH to authenticate with the repository I will use this. If you have been using a username and password to authenticate you must use the HTTPS address. 

Refer to the [GIT resource page](../resources/GIT.md) for instructions on setting up a SSH key. There are numerous advantages to this, the first one is security, and the second one is ease of use - If you use HTTPS you will need to enter your user/pass every time you do something with the repository, and if you decide to set the autosave function in git, your password will be stored as plain text on your computer which is not ideal.

![image-20200421202543400](assets/image-20200421202543400.png)



In my folder on my computer, clone the repository

```bash
[evan@gijon SpatialInfo]$ git clone git@gitlab.unimelb.edu.au:geom90042/student/evant1.git
Cloning into 'evant1'...
warning: You appear to have cloned an empty repository.
[evan@gijon SpatialInfo]$ 
```

Git has setup a new folder with the repository named after the repository name, in this case, it's my username. You can change this local folder name to anything you want and it won't affect your repository, I will name mine **myrepo**.

```bash
[evan@gijon SpatialInfo]$ mv evant1 myrepo
[evan@gijon SpatialInfo]$ ls -l
total 20
drwxr-xr-x 8 evan users 4096 Apr 20 09:17 geom90042
drwxr-xr-x 3 evan users 4096 Apr 21 20:30 myrepo
drwxr-xr-x 5 evan users 4096 Apr  1 09:32 studentarea
drwxr-xr-x 8 evan users 4096 Apr 19 13:17 teaching_resources
drwxr-xr-x 3 evan users 4096 Apr  1 08:37 testarea
[evan@gijon SpatialInfo]$ cd myrepo
```

 Git is also warning me that the repository is empty - this is expected.



## Adding files to git

The first thing to do to a git repository is add a README.md file. On the front page of a github or gitlab repository, this is shown to whoever reads the page. The language we use in this file is typically markdown, just like this tutorial file. It doesn't matter how you make this file, just as long as it's called README.md and has some basic information about your repository. I've decided to use the terminal text editor called **nano**, this should be installed already in Linux and MacOS. Something like notepad/notepad++ is fine, or whatever you have been writing your code in.

```bash
[evan@gijon myrepo]$ nano README.md
```

```bash
  GNU nano 4.9.2                          README.md                          Modified  
# evant1

My student repository for GEOM90042















^G Get Help   ^O Write Out  ^W Where Is   ^K Cut Text   ^J Justify    ^C Cur Pos
^X Exit       ^R Read File  ^\ Replace    ^U Paste Text ^T To Spell   ^_ Go To Line
```

Save the file (Ctrl+X to exit, and then y for yes).

Now we are ready to commit our first file to the repository. The way we can think about this is that a commit is a box that you've put things in. You want to add the items you want contained in it, and then a concise message that illustrates what's inside. We typically only write subject headings for our messages, that are limited to 50 characters, and are unique about what's changed so if we look through lots of commits we don't have to dig deeper to find the one we want. The expectation is that they are written in the imperative tense (ie. Release version 2.2, Update README documentation, Remove unused class) see https://chris.beams.io/posts/git-commit/ for examples.

We can check with git to see if it notices any changes with **git status**

```bash
[evan@gijon myrepo]$ ls -l
total 4
-rw-r--r-- 1 evan users 46 Apr 21 20:38 README.md
[evan@gijon myrepo]$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        README.md

nothing added to commit but untracked files present (use "git add" to track)
[evan@gijon myrepo]$ 
```

So it knows already there's a file, but it's not tracking it. So let's tell git about our new file.

```bash
[evan@gijon myrepo]$ git add README.md 
[evan@gijon myrepo]$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   README.md

[evan@gijon myrepo]$ 
```

So now it is tracking the README.md file. That is the only file we want included in our commit "box", so lets give it a label "Update initial README doc".

```bash
[evan@gijon myrepo]$ git commit -m "Update initial README doc"
[master (root-commit) 91a040c] Update initial README doc
 1 file changed, 3 insertions(+)
 create mode 100644 README.md
[evan@gijon myrepo]$ 

```

So that's it! The changes have been made, git has placed the commit away for us and signed it. 

It is important to know that git does not have to always be updated with the server. You can make 100s of commits, and then when you're ready, you can synchronise your local version with the one on gitlab. In a real-world environment, pushing your changes early allows anyone else in the repository to receive them and adapt their code to yours. At the moment, nothing has changed on our repository, but we can push our changes with a **git push**:

```bash
[evan@gijon myrepo]$ git push
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Writing objects: 100% (3/3), 269 bytes | 269.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
To gitlab.unimelb.edu.au:geom90042/student/evant1.git
 * [new branch]      master -> master
[evan@gijon myrepo]$ 

```

And now it's done! Our new file has been committed and pushed to the repository. Let's have a look.

![image-20200421205258546](assets/image-20200421205258546.png)



## Making further changes

So adding a file is pretty straightforward but git becomes very useful once you start working on a project. You can see all the changes you've made to a file, go back to previous revisions, merge in other people's changes to your code, and more. Let's have a look at making more changes and some basic uses of git that will help you with your project.

So I will change my README.md file to something a little bit nicer. I'll open **nano** (or Notepad, etc) again and make some changes.

```bash
  GNU nano 4.9.2                     README.md                     Modified  
# Evan Thomas (evant1)

My student repository for GEOM90042: Spatial Information Programming









^G Get Help    ^O Write Out   ^W Where Is    ^K Cut Text    ^J Justify
^X Exit        ^R Read File   ^\ Replace     ^U Paste Text  ^T To Spell

```

Save the file and exit.

Now if we run **git diff** it will tell us all the changes we have made to all of the files that git tracks

```bash
diff --git a/README.md b/README.md
index 6f0519e..019d28d 100644
--- a/README.md
+++ b/README.md
@@ -1,3 +1,3 @@
-# evant1
+# Evan Thomas (evant1)
 
-My student repository for GEOM90042
+My student repository for GEOM90042: Spatial Information Programming
```

The lines starting with "-" indicate the line has been removed and conversely with "+".

If we check our repository we can see one file has changed.

```bash
[evan@gijon myrepo]$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
[evan@gijon myrepo]$ 
```

Before we do anything else, let's add the files we want to start off with assessment 3. Copy the **test_rasters.py** file from the **geom90042** repository into ours. Once you've done that, you can check again:

```bash
[evan@gijon myrepo]$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        test_rasters.py

no changes added to commit (use "git add" and/or "git commit -a")
[evan@gijon myrepo]$ 
```

Let's track our new test file and the updated changed in README.md.

```bash
[evan@gijon myrepo]$ git add README.md test_rasters.py 
```

And now, again, we see what's ready to be commited:

```bash
[evan@gijon myrepo]$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   README.md
        new file:   test_rasters.py

[evan@gijon myrepo]$ 
```

Let's commit and push to the repository:

```bash
[evan@gijon myrepo]$ git commit -m "Update README doc and add test boilerplate"
[master b3c43f4] Update README doc and add test boilerplate
 2 files changed, 70 insertions(+), 2 deletions(-)
 create mode 100755 test_rasters.py
 
[evan@gijon myrepo]$ git push
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 12 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 1.78 KiB | 1.78 MiB/s, done.
Total 4 (delta 0), reused 0 (delta 0), pack-reused 0
To gitlab.unimelb.edu.au:geom90042/student/evant1.git
   91a040c..b3c43f4  master -> master
[evan@gijon myrepo]$ 

```

And one last look at our repository shows us our changes.

![image-20200421210815953](assets/image-20200421210815953.png)



## See what has changed

Say we want to see what has changed so far we can check out **git log**

```bash
[evan@gijon myrepo]$ git log
commit b3c43f49c885c3593299e88b5cf1aa46a28ee188 (HEAD -> master, origin/master)
Author: Evan Thomas <evan@evanjt.com>
Date:   Tue Apr 21 21:06:56 2020 +1000

    Update README doc and add test boilerplate

commit 91a040cac18b564f58e7f77dd919a723eec18830
Author: Evan Thomas <evan@evanjt.com>
Date:   Tue Apr 21 20:48:10 2020 +1000

    Update initial README doc
[evan@gijon myrepo]$
```

We can see all of our changes since the very beginning of our repository. We can also use these commit hash IDs to check changes between two commits or our most recent version. Or, say we made a huge mistake, we can go backwards all the way to another commit, set up a new branch, independent of our main code, and write from there, with it affecting anything else.

A simple use of **git diff** again, can show us the differences between our most recent code and an older version of our code

```bash
[evan@gijon myrepo]$ git diff 91a040cac18b564f58e7f77dd919a723eec18830
diff --git a/README.md b/README.md
index 6f0519e..019d28d 100644
--- a/README.md
+++ b/README.md
@@ -1,3 +1,3 @@
-# evant1
+# Evan Thomas (evant1)
 
-My student repository for GEOM90042
+My student repository for GEOM90042: Spatial Information Programming
diff --git a/test_rasters.py b/test_rasters.py
new file mode 100755
index 0000000..d6a4596
--- /dev/null
+++ b/test_rasters.py
@@ -0,0 +1,68 @@
+#!/usr/bin/python3

...

[evan@gijon myrepo]$

```



There's a lot to learn with git, but the basics are simple. If you remember to commit often, you will be able to recover from big problems easily.

## Finally

One last thing to tie everything in together (that we won't go into detail with this semester), is that with test cases written for your code. It is possible to set up "Continuous Integration", so that when you make a new commit to your repository, the repository server will check your code, run your tests and email you if your code has suddenly broken. This helps you find problems along the way rather at the last minute when you submit your work for your client :)

