## GEOM90042: Spatial Information Programming

Welcome to the repository for GEOM90042: Spatial Information Programming. Here you will find assessment data and resources for the subject.

### Resources

- [GIT](./resources/GIT.md): How-to for using Git source control
- [ENVIRONMENTS](./resources/ENVIRONMENTS.md): How to set up a Conda Environment
- [PROJECT_STRUCTURE](./resources/PROJECT_STRUCTURE.md): Best practice for structuring a Data Science project folder

### Assessments

- [1: Introduction to GIS and Spatial Information Programming](./assessments/a1)
  - Weighting: 0% (Hurdle: Complete submission)
  - Time: 5 hours
 

- [2: Introduction to Spatial Programming - Trajectory analysis](./assessments/a2)
  - Weighting: 20%
  - Time 25 hours
 
- [3: Working with Rasters and Digital Elevation Models](./assessments/a3)
  - Weighting: 20%
  - Time 25 hours
 
- [4: Spatial Data Science: data input, manipulation, analysis and presentation](./assessments/a4)
  - Weighting: 50%
  - Time 50 hours
 
### Staff information

* Lecturer
  * Dr. Martin Tomko
  * Engineering Block B, B304 (Contact hours by appointment)
  * tomkom@unimelb.edu.au
* Tutor
  * Zexian Huang
  * zexianh@student.unimelb.edu.au

