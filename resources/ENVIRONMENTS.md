# Quick guide to reproducible Data Science projects with Conda Environments

#### Meta
Author: Martin Tomko

Version: 0.1

Date: 2020/03/05

Target: Anaconda 4.7+, Python 3.6+

## Introduction

This is a follow-up documentation, that should be read after first reading * Quick guide to a Data Science project structure*, contained in PROJECT_STRUCTURE.md. 
Here we document a *best practice* in maintaining a replicable Python environment, ie, managing project dependencies with Conda. Dependencies can quickly turn into a nightmare, if not managed properly, and starting from a clean environment.

## Resources

This document has been Inspired by many Web links, in particular:

- [Cookiecutter data science](https://drivendata.github.io/cookiecutter-data-science/)
- [DataSicence in practice](https://medium.com/data-science-in-practice/saving-the-environment-with-anaconda-ad68e603d8c5)

## 0. Create your Project folder structure 

You should create a new environment for any project - ie, a workspace, as per * Quick distilled guide to a Data Science project structure*, contained in PROJECT_STRUCTURE.md

This document details how you create a Conda environment specification that you can save as a `environment.yaml` file in the root folder of your project.

After you have created an empty project structure, you should create a Conda environment for this project, to manage library dependencies.

Navigate to your project:
```bash      
cd ~/my/project/path/myproject
```

## 1. Creating a Conda environment to manage dependencies

Conda is a package manager, that will resolve automatically also upstream dependencies (if possible). Sometimes this is hard - you may want to spearate your larger analysis into subprojects, to avoid conflicts ( if possible).

**Download Anaconda**

Download and install the Anaconda package manager for your operating system:

https://www.anaconda.com/distribution/#download-section

**Create your Conda project:**
```bash                       
conda create --name myproject python # create new env. This will create an empty environment in the central .conda envs path. Will use the defaul python version
```
Sometimes you need a specific Python version attached to the environment:
```bash
conda create --name myproject python=3.7 # create new env. This will create an empty environment in the central .conda envs path. Will use the specified python version (as deep as subversion number, so here matching also 3.7.2 for instance)
```

## 2. Activate your environment

Before you start coding, you should install the packages you need, and save the environment. To do this, **first activate the environment:**
```bash
source activate myproject # activate environment
```

On Windows and MacOS this will be:
```bash
conda activate myproject
```

You will notice that the command line prompt becomes pre-fixed with the name of your environment:
```bash
(myproject) $
```

## 3.Install the required packages:

You can now install all the dependencies of your project. Recommended -- put them all into a single command if possible, as this allows Conda to optimise upstream dependency resolution and speed of install.
```bash
conda install <package-name>   # install package(s).
```

If you want to automatically tell the installation to proceed, you can use the `-y` flag:

```bash
conda install -y <package-name> 
```

## 4.Save the environment specification

To make the environment reproducible, save the recipe:
```bash
(myenv)$conda env export > environment.yaml  # save env spec into the environment.yml file ( no need to create it first)
```

Saving the environment will alow you to re-use the specification elsewhere, and is key to reproducible science. You can pass the yaml file to someone else (or the whole project), to **reproduce your environment:**
```bash
cd someotherproject
$conda env create -f environment.yaml
```
The above approach saves the entire recipe, including all the dependency packages that the explicitly installed packages need, and often additional, system-psecific requirements. It is sometimes more prudent to explort just *from history*, the packages you have explicitly specified, and let conda resovle dependencies on the target system:

```bash
(myenv)$conda env export --from-history -n myenv > environment.yml
```

**Note:** this does not reproduce the project structure. Normally, you would share the entire project structure ( maybe based on a code repository -- without data), upload the data, and then run the command above to reproduce your  Conda environment with appropriate dependencies. Note on code repositories will follow.

**Note:** Do not mix Conda and Pip if possible. Yet, some packages are not available in Conda. You *can* use pip to install them, but it is best to avoid this. You will need to install pip to your conda environment, and then use pip install pointing to the right pip ( not your system specific pip). Run this when your environment is active!

```bash
(myenv)$conda install pip
/anaconda/envs/dsp/bin/pip install <package name>
```

## 5. Listing available Conda environments

You get the list of Conda environment available as:

```bash
conda env list
```
This allows you to check that your environment has been created.

## 6. Creating a Conda environment in the local project path

If you want to create the environment in the local path ( not great for sharing the project, because it downloads all the packages there), you can use --prefix. I suggest to make clear that the project name is different than project if you do this, so it appends the env, as below:
```bash
conda create --prefix ./myprojectenv                   
```

## 7. Removing environments

If you made a mistake, or no longer care for the environment, you can remove it:

```bash
conda remove --name myproject --all # if you created it in the central install, with --name

conda remove --prefix ./myproject --all # if you created it in the local install, with --prefix
```

## 8. Follow-up: Managing project source with GIT

After you have completed the steps above, it is a good idea to use GIT to manage your sources. Follow the instructions in `GIT.md`

## 9. Exiting an environment

If you wish to exit the Anaconda environment, you can deactivate it with the following command:

```bash
conda deactivate
```