\documentclass{article}

\usepackage{booktabs}

\begin{document}

paste the tables here

\end{document}


# in jupyter
# The formatting of the table using the default settings is less than ideal 
print(overlap_results_df.to_latex())

# We can improve on this by specifying the table's column format and the float format
print(overlap_results_df.to_latex(column_format='ccccccc', float_format=lambda x: '%.3f' % x))



Outputs:
\begin{tabular}{lrrrrr}
\toprule
{} &   jaccard &      dice &  volume\_similarity &  false\_negative &  false\_positive \\
\midrule
0 &  0.820124 &  0.901174 &           0.052253 &        0.074650 &        0.121771 \\
1 &  0.881379 &  0.936950 &          -0.013196 &        0.069192 &        0.056827 \\
2 &  0.842653 &  0.914608 &          -0.087509 &        0.123732 &        0.043542 \\
\bottomrule
\end{tabular}

\begin{tabular}{ccccccc}
\toprule
{} &  jaccard &  dice &  volume\_similarity &  false\_negative &  false\_positive \\
\midrule
0 &    0.820 & 0.901 &              0.052 &           0.075 &           0.122 \\
1 &    0.881 & 0.937 &             -0.013 &           0.069 &           0.057 \\
2 &    0.843 & 0.915 &             -0.088 &           0.124 &           0.044 \\
\bottomrule
\end{tabular}