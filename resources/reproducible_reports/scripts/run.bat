cd ../notebooks
jupyter nbconvert --exec --to latex --template ../templates/latex_nocode.tplx --output paper.tex paper.ipynb
#cp references.bib ./results/references.bib
pdflatex paper
bibtex paper
pdflatex paper
pdflatex paper
del *.aux
del *.out
del *.log
del *.bbl
del *.blg
