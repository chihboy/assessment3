# Quick distilled guide to a Data Science project structure 

#### Meta
Author: Martin Tomko

Version: 0.1

Date: 2020/03/05

Target: Anaconda 4.7+, Python 3.6+

## Introduction

Here we document a *best practice* in structuring your project folders, and managing their dependencies with Conda. Dependencies can quickly turn into a nightmare, if not managed properly, and starting from a clean environment.

It is also important to follow a good project structure, to be able to quickly locate a particular piece of work.

## Resources

This document has been Inspired by many Web links, in particular:

- [Cookiecutter data science](https://drivendata.github.io/cookiecutter-data-science/)
- [DataSicence in practice](https://medium.com/data-science-in-practice/saving-the-environment-with-anaconda-ad68e603d8c5)
- [Template project](https://github.com/djnavarro/newproject) inspirational project sstructure template by Prof Danielle Navarro. Taylored to research and data analysis in R. Note how DN recommends an additional README.md in each subfolder too!

## 1. Design and create of your Project folder structure 

You should create a new environment for any project - ie, a workspace. 

**Create a folder for your project** with a structure for your data, code, tests, and the environment:

```
- data/ # immutable, input data
- src/ # for source code 
    - lib/ # put any libraries and modules you code here. May be one level up, if this is the only use, and your notebooks are in `notebooks`.
    - 01_myproject.ipynb # or *.py files with `__main__` . For analysis/lab books, data wrangling and exploration. Main code here. The folder below (notebooks) tends to have a different content, but it depends. Follow a clear file naming scheme (see later)
- tests/ # automated tests of your libraries - such as unit tests.
- docs/ # (optional) more extensive docs, maybe a vignette
- notebooks/ # containing your tutorials, or reports. Use subfolders to distinguish them from your main analysis run 
- outputs/ # may contain code outputs, or figures -use subfolders as appropriate
    - figs/
    - results/ # e.g., new datasets
- reports/ # (optional) for your generated reports, or assignment outcomes
- models/ #  (optional) if you are doing some machine learning, youy may train models to use later, or use models of others
- conda-env/  # (optional) see below, in point ` Creating a Conda environment in the local project path` . Beware, may result in a large amount of files, and hogging space
- README.md # this is a documentation for your overall project
- LICENCE.d # (optional) this sets the conditions on how your work can be used by others
- CITATION.md # (Optional), may be part of the main README.md too - if you publish your work, you want to be acknowledge in the way specified here.
- environment.yml # you can name it the same as your project
```
## 2. Create an empty project structure and populate README

We will use command line to manage all the steps below. There may be minor differences in commands, this here is written on Mac OSX, and should be the same on Linux systems. Windows may use \ instead of /. Some commands may also differ. 

```bash
$ mkdir myproject # create project folder        
$ cd myproject
$ mkdir src data tests docs outputs notebooks
$ touch README.md
```

You should then go an add infromation into the README.md ASAP - at least what the project is about. This should be written in Markdown.

Follow a structure containing at least the following:

```bash
Author:
Date:
Version:
Project summary: #what is this for, what it does, why it does it that way, what are the outcomes
Installation: #A manual enabling a new user to replicate results.
How to/Manual: #How to use it.
Acknowledgments/References: #if needed.
```

A Markdown syntax cheatsheet is [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet). 

You can have additional READMEs in subfolders, if needed - for instance, to explain naming structure of data files, or what different library modules do.

## 3. Note on file naming conventions and coding style

A clear, well-strucutred naming convention is important.

Some recommendations:

- for ALL Python code and files containing Python code, follow [PEP8](https://www.python.org/dev/peps/pep-0008/), in particular the Module and Package [naming](https://www.python.org/dev/peps/pep-0008/#package-and-module-names) conventions:

> Modules should have short, all-lowercase names. Underscores can be used in the module name if it improves readability. Python packages should also have short, all-lowercase names, although the use of underscores is discouraged.

- It is important to name your notebooks or python files in a manner that is clear where to start, and what sequence to follow, such as:

```
notebooks/
    01_data_reading.ipynb
    02_data_preprocessing.ipynb
    03_data_summary_analysis.ipynb
    04_model_training.ipynb
    05_model_testing.ipynb
    06_analysis.ipynb
    07_visualizations_report.ipynb
    00_run_entire_workflow.ipynb
```

- It is important that the structure in your data is clear - what are your raw data, what are the results of pre-processing. 
You also may have datasets that relate to a particular date. It is then useful to ALWAYS follow the `YYYYMMDD_data_specification` convention

## 4. Creating a Conda environment to manage dependencies

Now you should create a Conda environment for this project, to manage library dependencies. Refer to *# Quick guide to reproducible Data Science projects with Conda Environments* in `ENVIRONMENTS.md`

