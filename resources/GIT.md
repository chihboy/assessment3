## Git

One element of this subject will be using git for the distribution of assessment data and will be also used within assessments later into the subjects. It is important that you become familiar with the basic concepts of git. In the first assessment you are introduced to the concept of cloning a repository to your local computer.

### Install

Follow this tutorial to get git setup on your machine if you do not have Git installed

https://www.atlassian.com/git/tutorials/install-git

### Repository

The class repository is at:

https://gitlab.unimelb.edu.au/geom90042/geom90042

You need to sign in using your university student name and password.

### Authentication

As the repository is not hosted publicly, you will need an invitation link to access it, please contact the tutor for access if you cannot log in.

For cloning the repository on your machine, you will need to also authenticate, there are two methods to do this.

1. You can generate an SSH key pair and submit the public copy to the Unimelb Bitbucket. You will not have to worry about using your username and password, as your machine will verify itself with this key. To generate a key you can follow [these instructions](https://inchoo.net/dev-talk/how-to-generate-ssh-keys-for-git-authorization/). This is generally the **preferred** way to authenticate in most git environments. To upload your SSH key after generation, copy the contents of the SSH public key (the one ending in .pub, not the private key!) at https://gitlab.unimelb.edu.au/profile/keys
2. Alternatively, you can set your username and password for the repository in plain text (generally a bad idea, hence option 1), when you access it from the command line. When you clone, for example, the repository on your machine, you will include the username and password in the address. See below.

To clone this repository (assuming you are following authentication method 1 above), in your git client (Git Bash in Windows, or in the command line on Linux/Mac), type

```bash
git clone git@gitlab.unimelb.edu.au:geom90042/geom90042.git
```

For those opting for method two, you need to include your username and password in the link, type:

```bash
git clone https://username:password@gitlab.unimelb.edu.au/geom90042/geom90042
```

where **username** and **password** relate to your university student username and password that you used to login to the gitlab.

The repository will be cloned to the folder geom90042 within the current directory that you're in when you run the script. To find this out you can type 

```bash
pwd
```

To view the files in the current directory type

```bash
ls
```

and to enter a directory type

```bash
cd <directory name>
```

Making sure that if there are spaces in the directory name you encompass the entire name within " ", and that the directory name is case sensitive, for example

```bash
cd "Spatial Programming"
```



### Links

* https://github.com/Gazler/githug
  * Githug is a command-line based game that helps you become familiar with the use of git by presenting problems that you might be confronted with. Everyone gets stuck with git, no matter how good you are at it, but practising the correct steps ahead of time will help you tackle them when you run into trouble with your code. You advance through levels in the game by using the right command. 
  * Clone the repository and follow the instructions on their repository README.
* https://rogerdudler.github.io/git-guide/
  * A straight-forward beginners guide with just the basics.

