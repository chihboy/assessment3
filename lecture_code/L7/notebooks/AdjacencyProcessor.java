package ch.uzh.geo.tomko.NetworkProcessing.AdjacencyProcess;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.geotools.data.DataStoreFactorySpi;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.FeatureSource;
import org.geotools.data.FeatureStore;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;


import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.feature.visitor.FeatureVisitor;
import org.geotools.graph.build.feature.FeatureGraphGenerator;
import org.geotools.graph.build.line.LineStringGraphGenerator;
import org.geotools.graph.path.DijkstraShortestPathFinder;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Graph;
import org.geotools.graph.structure.Node;
import org.geotools.graph.traverse.standard.DijkstraIterator;
import org.geotools.graph.traverse.standard.DijkstraIterator.EdgeWeighter;
import org.geotools.referencing.CRS;
import org.geotools.util.NullProgressListener;


import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.FeatureType;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.util.ProgressListener;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.index.SpatialIndex;
import com.vividsolutions.jts.index.strtree.STRtree;

public class AdjacencyProcessor {
	String infile;
	String outfile;
	SpatialIndex INDEX;
	FeatureSource source;
	private FeatureCollection<SimpleFeatureType, SimpleFeature> features;
	String eol = System.getProperty("line.separator");
	OutputStreamWriter outwriter =null;
	boolean dual;
	private HashSet<Integer> nodes = new HashSet<Integer>();
	private HashSet<String> adjacencies = new HashSet<String>();
	private GeometryFactory geomFactory = new GeometryFactory();
	private boolean nodeFirst = false;
	private SimpleFeatureType featureType;
	private boolean weights = false;
	/**
	 * Constructor, without noding parameter, defaults nodeFirst=false
	 * @param infile
	 * @param outfile
	 * @param dual
	 */
	public AdjacencyProcessor(String infile,String outfile,boolean dual){
		this.infile = infile;
		this.outfile = outfile;
		this.dual = dual;

	}

	/**
	 * Constructor with noding parameter.
	 * @param infile
	 * @param outfile
	 * @param dual
	 * @param nodeFirst
	 */
	public AdjacencyProcessor(String infile,String outfile,boolean dual, boolean nodeFirst, boolean geoweights){
		this.infile = infile;
		this.outfile = outfile;
		this.dual = dual;
		this.nodeFirst = nodeFirst;
		this.weights = geoweights;
	}
	
	public AdjacencyProcessor(String infile,String outfile,boolean dual, boolean geoweights){
		this.infile = infile;
		this.outfile = outfile;
		this.dual = dual;
		this.weights = geoweights;
	}

	public void executeAdjacencyProcess() throws Exception{
		this.initializeFeatureSource();
		this.buildSpatialIndex();
		if(this.dual){
			this.adjacencyProcessDual();	
		}else{
			this.adjacencyProcessPrimal();
		}
		this.writeOut();
	}





	private void initializeFeatureSource() throws IOException{
		URL shapeURL = null;
		File f = new File(this.infile);
		try {
			shapeURL = f.toURI().toURL();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// get feature results
		ShapefileDataStore store = new ShapefileDataStore(shapeURL);
		String name = store.getTypeNames()[0];

		this.source = store.getFeatureSource(name);
		if(this.nodeFirst){
			System.out.println("Nodding");

			ArrayList<MultiLineString> mls = new ArrayList<MultiLineString>();
			FeatureCollection fts = this.source.getFeatures();

			System.out.println("Features before nodding: "+fts.size());


			FeatureType ftp = fts.getSchema();
			if (MultiLineString.class.isAssignableFrom(ftp.getGeometryDescriptor().getType().getBinding()) || LineString.class.isAssignableFrom(ftp.getGeometryDescriptor().getType().getBinding())){
				FeatureIterator<SimpleFeature> fIT = fts.features();
				while(fIT.hasNext()){
					SimpleFeature ft = fIT.next();
					mls.add((MultiLineString)ft.getDefaultGeometry());
				}
			}

			List<LineString> ml = splitMLines(mls);
			Iterator<LineString> mlIT = ml.iterator();
			SimpleFeatureBuilder fb = null;
			try {
				fb = createFeatureBuilder();	
			} catch (NoSuchAuthorityCodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FactoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			FeatureCollection<SimpleFeatureType, SimpleFeature> collection = FeatureCollections.newCollection();
			int count = 1;
			while(mlIT.hasNext()){
				LineString g = mlIT.next();
				fb.add(g);
				fb.add(count+"");
				SimpleFeature feature = fb.buildFeature(null);
				collection.add(feature);
			}
			this.features = collection;
			System.out.println("Features after nodding: "+this.features.size());

		}else{
			this.features=this.source.getFeatures();

		}

		System.out.println("Found features:"+this.features.size());

	}



	// shapefile reader.
	/**
	 * @param String in path to shapefile
	 * @param String out path to output adjacency list 
	 */
	private void adjacencyProcessDual () throws Exception{

		FeatureType ft = this.source.getSchema();
		//tests for being of line types
		if (MultiLineString.class.isAssignableFrom(ft.getGeometryDescriptor().getType().getBinding()) || 
			LineString.class.isAssignableFrom(ft.getGeometryDescriptor().getType().getBinding())){
			FeatureIterator<SimpleFeature> firstiterator = this.features.features();

			//try {
			while( firstiterator.hasNext() ){	
				SimpleFeature feature = (SimpleFeature)firstiterator.next();

				String a = feature.getID().toString();
				//System.out.println("element: "+a+"/"+size);
				this.nodes.add(Integer.parseInt(a.substring(a.indexOf(".")+1)));

				Geometry geom = (Geometry)feature.getDefaultGeometry();
				Envelope searchenv = geom.getEnvelopeInternal();
				List<SimpleFeature> hits = this.INDEX.query( searchenv );

				if(!hits.isEmpty()){
					Iterator<SimpleFeature> seconditerator = hits.iterator();
					while ( seconditerator.hasNext() ){
						SimpleFeature comparedfeature = (SimpleFeature)seconditerator.next();
						Geometry compgeom = (Geometry)comparedfeature.getDefaultGeometry();	
						if ((geom.intersects(compgeom)) && (feature.equals(comparedfeature)==false)){
							String b = comparedfeature.getID().toString();
							String adj = (a.substring(a.indexOf(".")+1) + " "+ b.substring(b.indexOf(".")+1));
							adjacencies.add(adj);
						}
					}

				}  
			}
			System.out.println("Processing resulted in "+this.nodes.size()+" elements with "+adjacencies.size()+" edges");
		} else {
			System.out.print("not line dataset");
		}
	}

	private void writeOut() throws IOException{
		System.out.println("saving");
		try {
			this.outwriter = new OutputStreamWriter(new FileOutputStream(new File (this.outfile)));
			//outwriter.write("*Vertices "+elements.size()+"\n");
			this.outwriter.write("*Vertices "+this.nodes.size()+eol);
			Integer[] nds = this.nodes.toArray(new Integer[this.nodes.size()]);
			ArrayList<Integer> ndss = new ArrayList<Integer>();
			for(int i =0;i<nds.length;i++){
				ndss.add(nds[i]);
			}
			Collections.sort(ndss,new IntegerComparator());


			Iterator<Integer> it = ndss.iterator();
			while( it.hasNext()){
				int  i = it.next();
				//outwriter.write(s+"\n");
				outwriter.write(i+""+eol);
			}

			outwriter.write("*Edges"+eol);

			String[] edgs = this.adjacencies.toArray(new String[this.adjacencies.size()]);
			ArrayList<String> edgss = new ArrayList<String>();
			for(int i =0;i<edgs.length;i++){
				edgss.add(edgs[i]);
			}
			Collections.sort(edgss,new StringComparator());


			Iterator<String> itt = edgss.iterator();
			while( itt.hasNext()){
				String s = itt.next();
				outwriter.write(s+eol);
			}

		} finally {
			if (outwriter!=null){
				try {
					outwriter.flush();
					outwriter.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}

		}
	} 


	/**
	 * Method to insert SimpleFeatures into a spatial index
	 * @param features
	 * @return
	 * @throws IOException
	 */

	@SuppressWarnings("deprecation")
	private void buildSpatialIndex() throws IOException{
		FeatureCollection features = this.source.getFeatures();
		final SpatialIndex index = new STRtree();     

		features.accepts( new FeatureVisitor(){
			public void visit(Feature feature) {
				SimpleFeature simpleFeature = (SimpleFeature) feature;                
				Geometry geom = (Geometry) simpleFeature.getDefaultGeometry();
				Envelope bounds = geom.getEnvelopeInternal();
				if(!bounds.isNull() ){ // must be empty geometry?                
					index.insert( bounds,  feature );
				}
			}

			@Override
			public void visit(SimpleFeature feature) {
				// TODO Auto-generated method stub
				SimpleFeature simpleFeature = (SimpleFeature) feature;                
				Geometry geom = (Geometry) simpleFeature.getDefaultGeometry();
				Envelope bounds = geom.getEnvelopeInternal();
				if(!bounds.isNull() ){ // must be empty geometry?                
					index.insert( bounds,  feature );
				}


			}
		}, new NullProgressListener() );

		this.INDEX = index;
	}

	/**
	 * returns a collection of Features from an index built on a FeatureCollection containing a query Geometry (use for, for instance, point in polygon queries), potentially within a distance specified by the parameter, default  = 0
	 * @param index
	 * @param queryGeom
	 * @param bufferDist
	 * @return
	 * @throws Exception 
	 */
	private static FeatureCollection<SimpleFeatureType, SimpleFeature> getContainingFeatures(SpatialIndex index, Geometry queryGeom, Double bufferDist) {
		FeatureCollection<SimpleFeatureType, SimpleFeature> collection = FeatureCollections.newCollection();
		//final Geometry queryGeom = (Geometry)queryFeature.getDefaultGeometry();
		Envelope searchenv = queryGeom.getEnvelopeInternal();
		//System.out.println("search env:"+searchenv.getMinX()+","+searchenv.getMinY()+","+searchenv.getMaxX()+","+searchenv.getMaxY());
		searchenv.expandBy(bufferDist);
		//SpatialIndex indexx = buildSpatialIndex(features);

		List<SimpleFeature> hits = index.query( searchenv );
		if(!hits.isEmpty()){
			for(int i=0;i<hits.size();i++){
				final SimpleFeature feature = (SimpleFeature)hits.get(i);
				Geometry geom = (Geometry) feature.getDefaultGeometry();
				if(geom.getGeometryType().equals("MultiPolygon")){
					geom = geom.buffer(bufferDist);

				}
				if (geom.intersects(queryGeom)&& !collection.contains(feature)){
					collection.add(feature);
				}
			}
		} 
		//		else{
		//			System.out.println("no hits"+hits.size());
		//			//throw new Exception("no hits");
		//		}

		return collection;
	}

	private void adjacencyProcessPrimal () throws Exception{
		//FeatureCollection<SimpleFeatureType, SimpleFeature> fCollection = this.source.getFeatures();

		//create a linear graph generate
		LineStringGraphGenerator lineStringGen = new LineStringGraphGenerator();

		//wrap it in a feature graph generator
		FeatureGraphGenerator featureGen = new FeatureGraphGenerator( lineStringGen );

		//throw all the features into the graph generator
		FeatureIterator iter = this.features.features();
		try {
			while(iter.hasNext()){
				Feature feature = iter.next();
				featureGen.add( feature );
			}
		} finally {
			iter.close();
		}
		Graph graph = featureGen.getGraph();
		System.out.println("Graph info [edges]:"+graph.getEdges().size());
		exportGraphToNet(graph);
	}

	private void exportGraphToNet(Graph g){

		Collection<Edge> edges = g.getEdges();
		Collection<Node> nodes = g.getNodes();
		Iterator<Edge> edgesIt = edges.iterator();
		HashMap<Integer,Integer> edgeMap = new HashMap<Integer,Integer>();
		int count = 0;
		while(edgesIt.hasNext()){
			Edge e = edgesIt.next();




			int eid = e.getID();
			int na = e.getNodeA().getID();
			int nb = e.getNodeB().getID();
			//doing magic to satisfy pajek format - vertices starting from 1 and numbered consecutively, without gaps 
			if(edgeMap.containsKey(na)){
				na = edgeMap.get(na);
			}else{
				count++; //assures vertices starting from 1 
				edgeMap.put(na, count);
				na = edgeMap.get(na);
			}
			if(edgeMap.containsKey(nb)){
				nb = edgeMap.get(nb);
			}else{
				count++;
				edgeMap.put(nb, count);
				nb = edgeMap.get(nb);
			}

			if(!this.nodes.contains(na)) this.nodes.add(na);
			if(!this.nodes.contains(nb)) this.nodes.add(nb);

			String adj = (na + " "+ nb);	
			if(nb<na){
				adj = (nb + " "+ na);	
			}

			if(this.weights){
				SimpleFeature sf = (SimpleFeature) e.getObject();
				Geometry geom =(Geometry)sf.getDefaultGeometry();
				String length = (new DecimalFormat("#.######").format( geom.getLength()));
				//length = (float)(((int)(length*10000.0))/10000.0);
				adj = (na + " "+ nb+" "+length);	
				if(nb<na){
					adj = (nb + " "+ na+" "+length);	
				}
			}

			this.adjacencies.add(adj);
		}





	}

	private List<LineString> splitMLines(ArrayList<MultiLineString> mlines) {
		FeatureCollection<SimpleFeatureType, SimpleFeature> ncollection = FeatureCollections.newCollection();
		List<LineString> lines = new ArrayList<LineString>();

		for (MultiLineString mls : mlines) {
			for (int i = 0; i < mls.getNumGeometries(); i++) {
				lines.add((LineString)mls.getGeometryN(i));
			}
		}

		Geometry grandMls = this.geomFactory.buildGeometry(lines);
		Point mlsPt = this.geomFactory.createPoint(grandMls.getCoordinate());
		Geometry nodedLines = grandMls.union(mlsPt);

		lines.clear();

		for (int i = 0, n = nodedLines.getNumGeometries(); i < n; i++) {
			Geometry g = nodedLines.getGeometryN(i);
			if (g instanceof LineString) {
				lines.add((LineString)g);
			}
		}


		return lines;
	} 

	private SimpleFeatureBuilder createFeatureBuilder() throws NoSuchAuthorityCodeException, FactoryException{
		SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
		typeBuilder.setName("network");
		typeBuilder.add("geom", LineString.class);
		typeBuilder.add("FID",String.class);

		this.featureType = typeBuilder.buildFeatureType(); 
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(this.featureType); 
		return featureBuilder;
	}

	/**
	 * Helper function to store the noded feature collection into a shapefile.
	 * @throws IOException
	 */
	private void saveToShape() throws IOException{
		DataStoreFactorySpi factory = new ShapefileDataStoreFactory();
		Map<String,Serializable> createFts = new HashMap<String,Serializable>();
		File outFile = new File (this.outfile.replace(".net", ".shp"));
		createFts.put("url", outFile.toURI().toURL() );
		createFts.put("create spatial index",Boolean.TRUE);

		ShapefileDataStore newStore = (ShapefileDataStore) factory.createNewDataStore( createFts );
		newStore.createSchema(this.featureType);

		FeatureStore<SimpleFeatureType, SimpleFeature> store = (FeatureStore<SimpleFeatureType, SimpleFeature>) newStore.getFeatureSource(newStore.getTypeNames()[0]);
		DefaultTransaction transaction = new DefaultTransaction("trans");
		store.setTransaction(transaction);
		store.addFeatures(this.features);
		transaction.commit();
		transaction.close();
	}

	/**
	 * Returns <code>load(filename, new SparseGraph?())</code>.
	 * @throws IOException?
	 *
	 */
	//	   public Graph load(String filename) throws IOException
	//	   {
	//	       return load(filename, new SparseGraph());
	//	   }


	/**
	 * Creates a <code>FileReader?</code> from <code>filename</code>, calls
	 * <code>load(reader, g)</code>, closes the reader, and returns the
	 * resultant graph.
	 * @throws IOException?
	 *
	 */
	//	   public Graph load(String filename, Graph g) throws IOException
	//	   {
	//	       Reader reader = new FileReader(filename);
	//	       Graph graph = load(reader, g);
	//	       reader.close();
	//	       return graph;
	//	   }

	/**
	 * Calls  <code>load(reader, new SparseGraph?())</code>, closes the
	 * reader, and returns the resultant graph.
	 * @throws IOException?
	 *
	 */
	//	   public Graph load(Reader reader) throws IOException
	//	   {
	//	       Graph graph = load(reader, new SparseGraph());
	//	       reader.close();
	//	       return graph;
	//	   }
	/**
	 * <p>Creates a <code>Graph</code> based on the adjacency list
	 * gathered from a Reader. The data must be in the following format:</p>
	 *
	 * <pre>
	 * v_1 v_1
	 * v_2 v_1
	 * v_2 v_3
	 * v_3 v_1 ...
	 * </pre>
	 *
	 * <p>
	 * where <code>v_i</code> is a node label (ID) for vertex <code>i</code>
	 * Each line in the file defines an edge between the specified vertices.
	 * The vertices themselves are defined implicitly: if a label is read
	 * from the file for which no vertex yet exists, a new vertex is created
	 * and that label is attached to it.
	 * </p>
	 *
	 * <p>
	 * The end of the file may be artificially set by putting the string
	 * <code>end_of_file</code> on a line by itself.
	 * </p>
	 *
	 * @return the graph loaded with these vertices, and labelled with a
	 * StringLabeller?
	 * @throws IOException? May occur in the course of reading from a stream.
	 */
	//	   public Graph load(Reader reader, Graph g) throws IOException
	//	   {
	//	       Collection edge_constraints = g.getEdgeConstraints();
	//
	//	       if (directed)
	//	           edge_constraints.add(Graph.DIRECTED_EDGE);
	//	       else
	//	           edge_constraints.add(Graph.UNDIRECTED_EDGE);
	//
	//	       VertexGenerator vg = new TypedVertexGenerator(edge_constraints);
	//
	//	       BufferedReader br = new BufferedReader(reader);
	//
	//	       while (br.ready()){
	//	           // read the line in, break it into 2 parts (one for each vertex)
	//	           String curLine = br.readLine();
	//	           if (curLine == null || curLine.equals("end_of_file"))
	//	               break;
	//	           if (curLine.trim().length() == 0)
	//	               continue;
	//
	//	           String[] parts = curLine.trim().split("\\s+");
	//
	//	           if (parts.length != 2){
	//	               System.err.println("Need two nodes per line!\n" + curLine);
	//	               continue;
	//	           }
	//
	//	           // fetch/create vertices for each part of the string
	//	           Vertex v_a = getOrCreateVertexByName(g, vg, parts[0]);
	//	           Vertex v_b = getOrCreateVertexByName(g, vg, parts[1]);
	//
	//	           Edge e = v_a.findEdge(v_b);
	//	           boolean absent = (e == null);
	//
	//	           if (absent){
	//	               if (directed)
	//	                   e = g.addEdge(new   DirectedSparseEdge(v_a, v_b));
	//	               else
	//	                   e = g.addEdge(new UndirectedSparseEdge(v_a, v_b));
	//	           }
	//	       }
	//	       br.close();
	//	       reader.close();
	//	       return g;
	//	   }
	//
	//	   private Vertex getOrCreateVertexByName(Graph g, VertexGenerator vg, String label){
	//
	//	       StringLabeller vID_label = StringLabeller.getLabeller(g);
	//	       Vertex v = (Vertex) vID_label.getVertex(label);
	//
	//	       if (v == null){
	//	           v = (Vertex)vg.create();
	//	           g.addVertex(v);
	//	           try{
	//	               vID_label.setLabel(v, label);
	//	           }
	//	           catch (UniqueLabelException e1) {
	//	               System.err.println(e1.getMessage());
	//	           }
	//	       }
	//	       return v;
	//	   }


}

