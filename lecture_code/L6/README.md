# Raster lecture for GEOM90042 Spatial Information Programming course on the 14th of April, 2021:

In this repository are the materials for the guest lecture that Jonathan Garber has delivered fro Martin Tomko for the GEOM90042 Spatial Information Programming course on the 14th of April, 2021.

**Note** the data for the archived lecture (Rasterio and pysheds-archive.ipynb) did not fit in the repo, so use it as a reference only.

## Setting up the virtual environment:

TO set up the virtual environment, please see the [setting_up_the_repo.md](setting_up_the_repo.md)