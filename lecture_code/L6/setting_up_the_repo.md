# Setting up the environment:

There are two ways to set up the environment for this repository. We have a standard anaconda requirements file, but I was also using poetry
as a dependency manager. This is because, when you add a package to poetry, it automatically updates the package list stored in the poetry.lock file!

Run all of these terminal commands in the repository folder.

## To do this without poetry:
create a new environment and point the file to the "conda_requriements.txt

```bash
conda create -n raster_lecture --file conda_requirements.txt
```

## to do this with poetry, create and activate the environment first:

```bash
conda create -n raster_lecture python=3.9
```
then activate said environment

```bash
conda activate raster_lecture
```

## Install poetry and run dependencies:

```bash
conda install poetry
```

this will install the poetry package, then you have to run one more command to install all of the packages stored in the poetry.lock file:

```bash
poetry install
```

This will install all of these within the virtual environment you have created. To add a new python library just use the following command:

```bash
poetry add package_1 package_2
```
and this will install the proper version of package_1 and package_2 in the environment, as well as update the package index in the poetry.lock file. There is lots more you can do with poetry. Check out the docs here: [https://python-poetry.org/](https://python-poetry.org/)


