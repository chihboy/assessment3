# This project demonstrates the use of Jupyter notebooks to produce reports, using ipypublish

Source: https://ipypublish.readthedocs.io/en/latest/getting_started.html

## Installation

Create a new conda environment with at least:

- pandoc
- jupyter
- ipypublish

```
$ conda create --name ipyreport -c conda-forge jupyter pandoc ipypublish # may need pandoc=2.6
$ source activate ipyreport
```

It is wise to preserve the resulting environment in a spec:
```
$ conda env export > environment.yaml
```

## Conversion

For conversion, `nbpublish` command line command is used.

```
nbpublish -le
```
provides all available configurations.