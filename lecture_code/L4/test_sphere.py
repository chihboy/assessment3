import unittest
## tests run here

from sphere import sphereVolume

class TestNotebook(unittest.TestCase):
    # additional setup up possible here
    def test_sphereVolume(self):
        # first test, for a known value of the square area
        # rounded to 2 decimals
        self.assertEqual(round(sphereVolume(1),1), 4.2)     

if __name__ == '__main__':
    unittest.main()