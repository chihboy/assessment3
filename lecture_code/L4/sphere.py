pi = 22/7
def sphereArea(r):
    '''
    float -> float
    returns the surface area of a sphere with radius r
    '''
    area = 4 * pi * r**2
    return area


def sphereVolume(r):
    '''
    float -> float
    returns the volume of a sphere with radius r
    '''
    volume = (4/3) * pi * (r**3)
    return volume
