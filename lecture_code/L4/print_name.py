#!/usr/bin/python3

# this is often not compulsory, but if we want to get arguments in, it is necessary
import sys # import sys, built-in library to work with the operating system.


# it is customary to have functions on the top of your script file ( or on the top of the notebook)
def text_decorator(mytext):
    nice_text = 'Your unhonourable name is:{0}'.format(mytext)
    print(nice_text)

def print_text_separator():
    print(f'=================================')


def main(args):
    '''
    The main method
    Usually very brief, an has a sequence of functions that execute.
    '''
    print("here we are in the main method")  
    print_text_separator()       
    if len(args[1:]) > 0:
        text_decorator(str(args[1]))
    else:
        print(f'No name provided, sorry.')
    print_text_separator()

if __name__ == "__main__":
    '''
    When python finds this line in the script 
    (usually put in the last place)
    It auto-executes it's content. 
    It is customary to call this with a single function, main()
    '''
    # we only work with the arguments from the second onwards ( they are a list)
    # this is becasue the first one is the file name of the script
    main(sys.argv)
    