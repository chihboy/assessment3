pi=22/7

def sphereArea(r):
    '''returns the surface area of a sphere with radius r

        Parameters
        ----------
        r : float
            radius of the sphere

        pi (constant): a constant, defined as 22/7

        Returns
        -------
        area
            the area of the sphere

    '''
    area = 4 * pi * r**2
    return area


def sphereVolume(r):
    '''returns the volume of a sphere with radius r

        Parameters
        ----------
        r : float
            radius of the sphere

        pi (constant): a constant, defined as 22/7

        Returns
        -------
        volume
            the volume of the sphere

    '''
    volume = (4/3) * (pi * r ** 3)
    return volume
