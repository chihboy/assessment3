#!/bin/bash
cd ../notebooks
jupyter nbconvert --exec --to latex --template ../templates/latex_nocode.tplx --output L6_raster_handling.tex L6_raster_handling.ipynb
#cp references.bib ./results/references.bib
pdflatex L6_raster_handling
bibtex L6_raster_handling
pdflatex L6_raster_handling
pdflatex L6_raster_handling
rm -f *.aux *.out *.log *.bbl *.blg
