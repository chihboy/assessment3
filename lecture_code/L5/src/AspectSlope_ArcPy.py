 # https://github.com/Esri/raster-functions/blob/master/functions/AspectSlope.py
 def updatePixels(self, tlc, shape, props, **pixelBlocks):
        dem = np.array(pixelBlocks['raster_pixels'], dtype='f4', copy=False)[0]                     # Input pixel array.
        m = np.array(pixelBlocks['raster_mask'], dtype='u1', copy=False)[0]                         # Input raster mask.
        self.noData = self.assignNoData(props['pixelType']) if not(props['noData']) else props['noData']
        xKernel = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])                                    # Weights for calculation of dz/dx value array.
        yKernel = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])                                    # Weights for calculation of dz/dy value array.
        temp = np.where(np.not_equal(dem, self.noData), dem, dem)
        p = props['cellSize']
        deltaX = (ndimage.convolve(temp, xKernel)) / 8                                              # dz/dx values.
        deltaY = (ndimage.convolve(temp, yKernel)) / 8                                              # dz/dy values.
        if (p[0] <= 0) | (p[1] <= 0):
            raise Exception("Input raster cell size is invalid.")
        dx = deltaX / p[0]                                                                          # Divide by cell size.
        dy = deltaY / p[1]
        dx = dx * self.zf                                                                           # Multiply by z-factor.
        dy = dy * self.zf
        slopeTangent = (np.sqrt((dx * dx) + (dy * dy))) * 100                                       # Slope Calculation.
        aspect = 57.29578 * np.arctan2(deltaX, (-1) * deltaY)                                       # Aspect Calculation.
        aspect[(aspect < 0.00)] = (360.00 - (90.00 - aspect[(aspect < 0.00)])) + 90
        aspect[slopeTangent == 0] = -1                                                              # Aspect assigned -1 for slope values 0.
        slopeTangent[(slopeTangent >= 0) & (slopeTangent < 5)] = -10
        slopeTangent[(slopeTangent >= 5) & (slopeTangent < 20)] = -20
        slopeTangent[(slopeTangent >= 20) & (slopeTangent < 40)] = -30
        slopeTangent[(slopeTangent >= 40)] = -40
        slopeTangent[(slopeTangent == -10)] = 10
        slopeTangent[(slopeTangent == -20)] = 20
        slopeTangent[(slopeTangent == -30)] = 30
        slopeTangent[(slopeTangent == -40)] = 40
        aspect[(aspect >= -1) & (aspect <= 22.5)] = 1
        aspect[(aspect > 22.5) & (aspect <= 67.5)] = 2
        aspect[(aspect > 67.5) & (aspect <= 112.5)] = 3
        aspect[(aspect > 112.5) & (aspect <= 157.5)] = 4
        aspect[(aspect > 157.5) & (aspect <= 202.5)] = 5
        aspect[(aspect > 202.5) & (aspect <= 247.5)] = 6
        aspect[(aspect > 247.5) & (aspect <= 292.5)] = 7
        aspect[(aspect > 292.5) & (aspect <= 337.5)] = 8
        aspect[(aspect > 337.5) & (aspect <= 360)] = 1
        finalArray = np.add(slopeTangent, aspect)                                                   # Add the slope and aspect arrays.
        finalArray[(finalArray >= 11) & (finalArray <= 18)] = 19
        pixelBlocks['output_pixels'] = finalArray[1:-1, 1:-1].astype(props['pixelType'])
        pixelBlocks['output_mask'] = \
            m[:-2, :-2]  & m[1:-1, :-2]  & m[2:, :-2]  \
            & m[:-2, 1:-1] & m[1:-1, 1:-1] & m[2:, 1:-1] \
            & m[:-2, 2:] & m[1:-1, 2:] & m[2:, 2:]

        return pixelBlocks
