# How to use the Conda ( and Metis server) environment:

## Create a fully functional environment

### Create your home directory

For instance, something like:

`$ mkdir GEOM90042
 $ cd GEOM90042
 $ mkdir asignment1
 $ cd assignment1
`


## Create your environment

You can create it from scratch:
``` 
$conda create --name myenv python=3.7 
``` 

This can be any `Python` version, or not even specified)

Then, you activate the environment:

``` 
$conda activate myenv 
```

## Save your environment into a yml file:


## Create your environment from a pre-prepared yml file:

If someone provided you with the environment.yml file to re-create their environment, you can use it this way:

```
conda env create -f environment. yml
```

Note - you can investigate the yml file, and see - there may be issue,s if it was created not with the `--from-history` flag. In that case, you can manually purge away the versions of packages you are not confident about.

## Enable kernels in your environment 

*Note:* this is of particular relevance on Metis, you should not need to do this at home.

To enable jupyterlab to see all the installed packages, a small trick is needed (https://kegui.medium.com/how-to-add-conda-environment-to-jupyter-lab-step-by-step-431cd2c47708)[https://kegui.medium.com/how-to-add-conda-environment-to-jupyter-lab-step-by-step-431cd2c47708]:

``` $conda create --name myenv python=3.7 ``` 
(let's say, it can be any Python, or not even specified)

``` 
$conda activate myenv
$conda install ipykernel
$ python -m ipykernel install --user --name=myenv 
```
(strictly speaking, this name does not have to be the same, but keep it the same for clarity)
Then, again:

```

$ conda deactivate
$ conda activate myenv
```

Yes, that is right - you want to `restart` your environment after this step.

To remove an unwanted kernel specification, use:

``` jupyter kernelspec remove <kernel-name> ```

Then, you can open a notebook in the user interface, and on the right, when you click on the running kernel, you will have a drop down menu. Pick Python (myenv). This way, you may have different kernels for each assignment.

### Then, you install additional packages
You can insert the Channel from which the pckage is installed

``` conda install {-c conda-forge} packagename```
