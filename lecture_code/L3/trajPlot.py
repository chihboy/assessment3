# the code is largely adapted from:https://anaconda.org/jbednar/nyc_taxi/notebook

from bokeh.plotting import figure, output_notebook, show
from bokeh.tile_providers import get_provider, STAMEN_TONER
from bokeh.palettes import Spectral11

tile_provider = get_provider(STAMEN_TONER)

def base_plot(x_range, y_range, tools='pan,wheel_zoom,reset'):

    # Our bounding box in Sydney
    
    plot_width  = int(750)
    plot_height = int(plot_width//1.2)
    p = figure(tools=tools, plot_width=plot_width, plot_height=plot_height,
        x_range=x_range, y_range=y_range)
        # , outline_line_color=None,
        # min_border=0, min_border_left=0, min_border_right=0,
        # min_border_top=0, min_border_bottom=0)

    p.axis.visible = False
    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = None
    return p

def PlotPoints(p_x,p_y, c = 'red', step = 10):
    x_range, y_range = (min(p_x)-10,max(p_x)+10), (min(p_y)-10,max(p_y)+10)
   
    p = base_plot(x_range, y_range)
    p.add_tile(tile_provider)
    for i in range(0,len(p_x),step):
        #print (i, len(p_x))
        p.circle(x=p_x[i], y=p_y[i], color = c)
    show(p)
