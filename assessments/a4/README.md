# Assessment 4:
## Spatial Data Science: data input, manipulation, analysis and presentation


- Weighting: 50%
- Time estimated: 50 hours
- Groups of 2 or 3

## Contents:

- **a4.pdf** : Assessment specification
- **data/VicRoadsAccidents** : six years of traffic incident data from VicRoads
- **data/RegionsLGA_2017** : ABS LGA boundaries
- **data/RegionsSA2_2016** : ABS SA2 boundaries
