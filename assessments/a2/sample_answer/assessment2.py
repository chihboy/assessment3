#!/usr/bin/env python3

'''
Python 3 script written for Assessment #2
of GEOM90042: Spatial Information Programming

This is only one way of approaching the assessment, there
can be many other ways.

Please use this only as a guide. This has been adapted
from previous years and may not reflect exactly the specified
requirements.

Author:   Evan Thomas
Email:    evant1@student.unimelb.edu.au
Date:     28/04/2020
'''

import csv
import sys
import os
from pyproj import Transformer
from scipy.spatial import distance
from datetime import datetime, timedelta


def import_csv(filename):
    '''
    Imports csv file, returns a list

    Function assumes there are no blank cells in the data, if
    there are any, raise an exception
    '''

    try:
        with open(filename, 'r') as inFile:
            data = list(csv.reader(inFile))
        if check_csv_blank_cells(data):
            raise ValueError
    except OSError as err:  # Handle file errors
        sys.exit("OS error:{0}".format(err))
    except ValueError:  # Handle missing cells
        sys.exit("The CSV file contains blank cells in above rows")

    return data


def check_csv_blank_cells(csv_list):
    '''
    Scans through a 2D list created by a csv import for blank cells.
    Prints all rows containing blank cells and returns 1 for blank
    cells, 0 for none for error handling.
    '''

    cell_error = 0
    for row in csv_list:
        for cell in row:
            if not cell:
                print(row)
                cell_error = 1
    return cell_error


def exportcsv(filename, header, data):
    try:
        with open(filename, 'w') as inFile:  # Remove old file, create new
            output = csv.writer(inFile)
            output.writerow(header)
            output.writerows(data)
    except OSError as err:
        sys.exit("OS error:{0}".format(err))  # Exit if error overwriting csv


def project_coordinate(from_epsg, to_epsg, in_x, in_y):
    '''
    Reproject one coordinate, force always_xy to maintain axis no matter the
    projection system
    '''

    transform_module = Transformer.from_crs(from_epsg,
                                            to_epsg,
                                            always_xy=True)

    return transform_module.transform(in_x, in_y)


def proj_coord(csv, x_pos, y_pos, fromproj, toproj):
    '''
    Function to project x & y coordinates from a list.
    Requires the list name, location of x
    and y indexes in the list, and the input/output
    EPSG coordinate systems. A new list is
    returned with appended transformed coordinates.
    '''

    for row in csv:
        x, y = project_coordinate(fromproj, toproj, row[x_pos], row[y_pos])
        row.append(x)
        row.append(y)

    return csv  # Return altered list


def compute_distance(x1, y1, x2, y2):
    '''
    Measures distance between two points in a projected system
    using euclidean distance from SciPy.
    '''

    a = (float(x1), float(y1))
    b = (float(x2), float(y2))
    return distance.euclidean(a, b)


def calc_time_dif(beginning, end):
    '''
    Calculates the time difference between two intervals
    in HH:MM:SS using datetime module.
    Function assumes time interval does not exceed 24 hours.
    '''

    time_format = '%H:%M:%S'  # Set the input time format
    time_difference = (datetime.strptime(end, time_format)
                       - datetime.strptime(beginning, time_format))

    # This will prevent output from returning '-1 day' if it crosses midnight
    if time_difference.days < 0:
        time_difference = timedelta(days=0,
                                    seconds=time_difference.seconds,
                                    microseconds=time_difference.microseconds)

    return time_difference.total_seconds()


def calc_speed(distance, time):
    '''
    Calculates speed as a function of distance over time in seconds.
    Assuming this is Python 3 for integer division
    '''

    return distance/time


def task1(in_csv, out_csv, in_proj, out_proj):
    in_csv_path = os.path.join(os.getcwd(), in_csv)
    out_csv_path = os.path.join(os.getcwd(), out_csv)

    csvlist = import_csv(in_csv_path)
    header = csvlist[0]  # Get header
    points = csvlist[1:]  # Get data

    # Search the header for the index values
    y_pos = header.index('latitude')
    x_pos = header.index('longitude')
    time_pos = header.index('time')
    traj_pos = header.index('trajectory_id')
    node_pos = header.index('node_id')

    # Convert points from csv into projected coordinate system
    newpoints = proj_coord(points, x_pos, y_pos, in_proj, out_proj)
    header.append("X_UTM")
    header.append("Y_UTM")
    y_pos = header.index('Y_UTM')
    x_pos = header.index('X_UTM')
    exportcsv(out_csv_path, header, newpoints)
    indices = (y_pos, x_pos, time_pos, traj_pos, node_pos)

    return newpoints, indices


def task2(newpoints, indices, outfile):
    y_pos, x_pos, time_pos, traj_pos, node_pos = indices  # Unpack indices
    outfile_path = os.path.join(os.getcwd(), outfile)

    # Find the maximum number of trajectories
    max_trajectory = 0
    for i in newpoints:
        if int(i[traj_pos]) > max_trajectory:
            max_trajectory = int(i[traj_pos])

    trajectory_list = []  # Create 3D list
    for i in range(max_trajectory+1):
        trajectory_templist = []
        for row in newpoints:
            if int(row[traj_pos]) == i:
                trajectory_templist.append(row[1:])
        trajectory_list.append(trajectory_templist)

    trajectory_count = 0
    trace_buffer = 1

    total_trace_length_all = 0
    longest_trace = ()  # Holds the longest trace as (index, distance, time)

    with open(outfile_path, "w") as outfile:
        # Run through each trajectory
        for trajectory in trajectory_list:
            max_nodes = 0
            sum_distance = 0.0
            sum_seconds = 0.0
            max_node_distance = (0, 0)
            min_speed = ()  # Make tuple for node min speed (index, speed)
            max_speed = ()  # Make tuple for node max speed (index,speed)

            # Count the amount of nodes for current trajectory
            for node in trajectory:
                max_nodes += 1
            node_segment = max_nodes - 1

            # Run through each node in the current trajectory
            # and perform the calculations
            for node in trajectory:
                x_pos = len(node) - 2
                y_pos = len(node) - 1
                if int(node[0]) == 0:
                    previous_node = node  # Store first value for  next node
                else:
                    # Distance measurement
                    node_distance = compute_distance(previous_node[x_pos],
                                                     previous_node[y_pos],
                                                     node[x_pos], node[y_pos])
                    sum_distance = sum_distance + node_distance

                    # Find the segment of the longest distance, with
                    # its index, and form a tuple (index, distance)
                    if node_distance > max_node_distance[1]:
                        max_node_distance = (int(node[0]), node_distance)

                    # Calculate time per each segment
                    # measurement, then add it to sum variable
                    node_time_dif = calc_time_dif(previous_node[4], node[4])
                    sum_seconds = sum_seconds + node_time_dif

                    # Store this node in memory for the
                    # calculation of the next segment
                    previous_node = node

                    # Calculate node speed, and check if it's the
                    # highest or lowest in the trajectory
                    node_speed = calc_speed(node_distance, node_time_dif)
                    if not (min_speed) or (node_speed < min_speed[1]):
                        min_speed = (int(node[0]), node_speed)
                    if not (max_speed) or (node_speed > max_speed[1]):
                        max_speed = (int(node[0]), node_speed)

            # Index values are decreased by 1 in these print
            # outs to start segment indexes at 0
            trajectory_string = ("Trajectory {}'s length is {:.2f}m.\n"

                                 "The length of its longest segment is "
                                 "{:.2f}m, and the index is {}.\n"

                                 "The average sampling rate for the "
                                 "trajectory is {:.2f}s.\n"

                                 "For the segment index {}, "
                                 "the minimal travel speed is reached.\n"

                                 "For the segment index {}, "
                                 "the maximum travel speed is reached.\n"
                                 "----\n")
            outfile.write(trajectory_string.format(
                str(trajectory_count + trace_buffer),
                sum_distance,
                max_node_distance[1],
                max_node_distance[0],
                sum_seconds/node_segment,
                min_speed[0], max_speed[0]))

            total_trace_length_all += sum_distance
            if not (longest_trace) or (longest_trace[1] < sum_distance):
                longest_trace = (trajectory_count, sum_distance, sum_seconds)
            trajectory_count += 1  # Increment count before next trajectory

        outfile.write("The total length of all "
                      "trajectories is {:.2f}m.\n".format(
                        total_trace_length_all))

        outfile.write("The index of the longest trajectory is {}, and the "
                      "average speed along the trajectory "
                      "is {:.2f}m/s.\n".format(int(longest_trace[0])+1,
                                               calc_speed(longest_trace[1],
                                                          longest_trace[2])))
        outfile.write("Program complete")


def main():
    in_csv = 'trajectory_data.csv'
    out_csv = 'assessment2.csv'
    out_txt = 'assessment2_out.txt'
    in_proj = 4326
    out_proj = 4796

    newpoints, indices = task1(in_csv, out_csv, in_proj, out_proj)
    task2(newpoints, indices, out_txt)

    print("Program complete")


if __name__ == "__main__":
    main()
