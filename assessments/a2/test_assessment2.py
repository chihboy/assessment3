#!/usr/bin/python3

''' This script runs a set of unittests on the Python
    code named assessment2.py written for the class
    GEOM90042: Spatial Information Programming.

    Execute by entering your Anaconda environment and
    typing from the command line:

    python test_assessment2.py -v

    This will test a few functions and their outputs
    for their correctness. '''

import unittest
import os
import assessment2


class TestTaskOne(unittest.TestCase):
    def setUp(self):
        ''' Sets up a few variables to run test cases in Task 1'''
        self.filename = os.path.join(os.getcwd(), "trajectory_data.csv")
        self.test_coordinate = (4326, 4796, 116.318417, 39.984702)
        self.test_coordinate_answer = (441782.79591610597, 4428131.247163864)

    def test_import_csv(self):
        ''' Test the import_csv function '''
        csv_output = assessment2.import_csv(self.filename)
        self.assertTrue(isinstance(csv_output, (list, dict, tuple)),
                        "Output not a list, tuple or dictionary. \
                        Ignore this error if you defined a class.")

    def test_project_coordinate(self):
        ''' Tests the project_coordinate function '''
        projected_coordinate = assessment2.project_coordinate(
            *self.test_coordinate)
        self.assertTrue(isinstance(projected_coordinate, tuple),
                        "Projected coordinate not a tuple")
        self.assertEqual(len(projected_coordinate), 2,
                         "Function does not return exactly 2 coordinates")
        self.assertTrue(isinstance(projected_coordinate[0], float),
                        "Projected X coordinate not a float, seems unlikely")
        self.assertTrue(isinstance(projected_coordinate[1], float),
                        "Projected Y coordinate not a float, seems unlikely")

        # Test X and Y projection coordinates with verified proj4 projection
        self.assertAlmostEqual(assessment2.project_coordinate(
            *self.test_coordinate)[0], self.test_coordinate_answer[0])
        self.assertAlmostEqual(assessment2.project_coordinate(
            *self.test_coordinate)[1], self.test_coordinate_answer[1])


class TestTaskTwo(unittest.TestCase):
    def setUp(self):
        self.test_coords_distance = (441782.79591610597, 4428131.247163864,
                                     440370.6660333868, 4428104.084778673)
        self.test_coords_distance_answer = 1412.3910934432351

    def test_compute_distance(self):
        ''' Test compute_distance function'''
        answer = assessment2.compute_distance(*self.test_coords_distance)
        self.assertTrue(isinstance(answer, float),
                        "Return value from distance calculation not float, \
                        seems unlikely")
        self.assertAlmostEqual(answer, self.test_coords_distance_answer)


if __name__ == '__main__':
    unittest.main()
