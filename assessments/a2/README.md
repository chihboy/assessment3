# Assessment 2: 
## Introduction to Spatial Programming - Trajectory analysis

- Weighting: 20%
- Time estimated: 25 hours

## Contents:

- **a2.pdf** : Assessment specification
- **trajectory\_data.csv** : input dataset
- **test_assessment2.py** : automated test script for your code