#!/usr/bin/python3

''' This script is boilerplate designed to write test
    cases for assessment 3 of the class
    GEOM90042: Spatial Information Programming.

    Execute by entering your Anaconda environment and
    typing from the command line:

    python test_assessment3.py -v

    The examples here are given from assessment 2, you are
    to create your own and replace the docstrings. Remember
    to keep it simple. The purpose of a unit test is to test
    each operation independently of all others in the program
    to help identify small mistakes.

    Please refer to https://docs.python.org/3/library/unittest.html
    '''

import unittest
import rasters


class TestTaskOne(unittest.TestCase): # Create a class name of your choice or leave it as this, but we need to include unittest.TestCase to import
    def setUp(self):
        ''' In this function you can define the variables you need to use
            in the functions for TestTaskOne. The name of this function is
            important as it's defined in the unittest library. It is, however,
            not compulsory for the operation of unittest. It can be deleted if
            you do not need to setup any variables.

            To define class variables,they need to have "self." appended
            to the beginning of them in order to be accessible by other
            functions in the class. Some examples from assessment 2 have
            been provided to allow insight into creating tests (please
            remove these in your submission) '''

        #self.filename = os.path.join(os.getcwd(), "trajectory_data.csv")       # Here we can use self.filename below
        #self.test_coordinate = (4326, 4796, 116.318417, 39.984702)             # Likewise for these variables
        #self.test_coordinate_answer = (441782.79591610597, 4428131.247163864)  # And this one too

    def test_import_csv(self):
        ''' Here we have defined a function that tests importing of a
            csv in assessment 2. It's important that your class functions
            include self in its parameters, and also that every test
            function you create, its name starts with "test_" '''

        #csv_output = assessment2.import_csv(self.filename)             # Imports CSV using the filename we setup in setUp()

        #self.assertTrue(isinstance(csv_output, (list, dict, tuple)),   # Assertion tests a true relationship between
                        "Output not a list, tuple or dictionary. \      # csv_output and whether it returns one of the three
                        Ignore this error if you defined a class.")     # types within the tuple


class TestTaskTwo(unittest.TestCase):
    ''' The same as above, but this time we're creating a new
    test class for task two, to categorise our functions '''
    def setUp(self):
        ''' Set up variables here, just like above '''

    def test_compute_distance(self):
        ''' Again, only an example from assessment 2,
        please remove for submission. Set up a function to
        test something in another task. '''

if __name__ == '__main__':
    unittest.main()
