# Assessment 3:
## Working with Rasters and Digital Elevation Models

- Weighting: 20%
- Time estimated: 25 hours

## Contents:

- **a3.pdf** : Assessment specification
- **test_rasters.py** : Boilerplate test script
- **DEM.zip** : An exported digital elevation model from Geoscience Australia
