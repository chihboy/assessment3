from pyproj import Transformer
import os
from PrettyTable import PrettyTable
import numpy as np
from rasterio.warp import calculate_default_transform, reproject, Resampling
import math
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import signal
import rasterio
from rasterio import plot


# read raster and convert it to dictionary
def summary_dem(file):
    # dictionary
    summary = ''
    try:
        # read raster
        with rasterio.open(file) as raster:
            # elevationband
            band = raster.read(1)

            # Get the boundary of the raster
            left = raster.bounds.left
            long = raster.bounds.right
            bottom = raster.bounds.bottom
            top = raster.bounds.top

            # Determine raster crs
            if (raster.crs == 'EPSG:4326'):
                # ces transformation
                trans = Transformer.from_crs(raster.crs, 'epsg:3111')
                min_x, min_y = trans.transform(bottom, left)
                max_x, max_y = trans.transform(top, long)
                # write info to dictionary
                summary = {'Filename' : file.split("./")[1],
                           'Coordinate system' : f"{raster.crs}",
                           'Min x, Min Lon' : f"{str(round(min_x,4))} m, {str(round(left,4))} degrees",
                           'Max x, Max Lon' : f"{str(round(max_x,4))} m, {str(round(long,4))} degrees",
                           'Min y, Min Lat' : f"{str(round(min_y,4))} m, {str(round(bottom,4))} degrees",
                           'Max y, Max Lat' : f"{str(round(max_y,4))} m, {str(round(top,4))} degrees",
                           'Width, Height, Cell size' : f"{str(round(raster.width,4))} px {str(round(raster.height,4))} px {str(round(raster.res[0],4))}",
                           'NoData' : str(raster.nodata),
                           'Min value, max value' : f"{str(round(band.min(),4))}, {str(round(band.max(),4))}"}

            elif (raster.crs == 'EPSG:3111'):
                trans = Transformer.from_crs(raster.crs, 'epsg:4326')
                min_y, min_x = trans.transform(bottom, left)
                max_y, max_x = trans.transform(top, long)
                summary = {'Filename' : file.split("./")[1],
                           'Coordinate system' : f"{raster.crs}",
                           'Min x, Min Lon' : f"{str(round(bottom,4))} m, {str(round(min_x,4))} degrees",
                           'Max x, Max Lon' : f"{str(round(top,4))} m, {str(round(max_x,4))} degrees",
                           'Min y, Min Lat' : f"{str(round(left,4))} m, {str(round(min_y,4))} degrees",
                           'Max y, Max Lat' : f"{str(round(long,4))} m, {str(round(max_y,4))} degrees",
                           'Width, Height, Cell size' : f"{str(round(raster.width,4))} px {str(round(raster.height,4))} px {str(round(raster.res[0],4))}",
                           'NoData' : str(raster.nodata),
                           'Min value, max value' : f"{str(round(band.min(),4))},{str(round(band.max(),4))}"}
       
    except FileNotFoundError:
        print("Wrong file or file path")

    return summary


# convert dictionary to lsit and plot it as PrettyTable
def display_summary(dict_summary):
    # table for outputting PrettyTable
    table = [[]]
    keylist = []
    valuelist = []

    for key in dict_summary.keys():
        keylist.append(key)
    for value in dict_summary.values():
        valuelist.append(value)

    table[0] = ['', '']
    for i, row in enumerate(keylist):
        table_ = [row, valuelist[i]]
        table.append(table_)

    new_table = PrettyTable(table, ['', ''])
    print(new_table._repr_latex_())


# Visualize the raster as a plot
def visualization(raster_path):
    try:
        # read raster
        with rasterio.open(raster_path, "r") as src:
            # elevationband
            band = src.read(1)
            # get the leftest and bottomest coordinate
            x_unit = abs(src.bounds.right-src.bounds.left) / len(band[0])
            y_unit = abs(src.bounds.top-src.bounds.bottom) / len(band)
            # convert raster elevation band to numpy array
            # and find the coordinate with the max value
            arr = np.array(band)
            result = np.argwhere(arr == band.max())
            for i, v in enumerate(result):
                print((src.bounds.left + v[1] * x_unit),
                      (src.bounds.top - v[0] * y_unit))

            # prepare the ax for x axis label and y axis label
            fig, ax = plt.subplots()

            # set the value and corresponding color in color bar
            levels = [70.18749, 175, 350, 700, 1400, 1500, 1563.1005]
            clrs = ['#3CFAFF', '#6A95FF', '#E61AFE8C',
                    '#f4ded9', '#eda14f', '#b55400', '#633b11']

            # set cmap as color bar
            cmap, norm = mpl.colors.from_levels_and_colors(levels,
                                                           clrs,
                                                           extend='max')
            # render a plot with color bar
            img = ax.imshow(band, cmap=cmap)
            fig.colorbar(img, spacing='proportional')

            # cover the plot with the the corrdinate map
            img = plot.show((src, 1), cmap=cmap, interpolation='none', ax=ax)
            # set xand y axis label
            ax.set_xlabel('Longitude')
            ax.set_ylabel('Latitude')

    except FileNotFoundError:
        print("Wrong file or file path")


# resample and reproject the raster
def dataset_conversion(value, output_name):
    summary = ''
    try:
        with rasterio.open(os.path.join(os.path.curdir, "CLIP.tif")) as src:
            # get input coordinate system
            Input_CRS = src.crs
            # define the output coordinate system
            Output_CRS = "EPSG:3111"
            # set up the transform and essential arguments
            Affine, Width, Height = calculate_default_transform(Input_CRS,
                                                                Output_CRS,
                                                                src.width * value,
                                                                src.height * value,
                                                                *src.bounds)
            kwargs = src.meta.copy()
            kwargs.update({'crs': Output_CRS,
                           'transform': Affine,
                           'affine': Affine,
                           'width': Width,
                           'height': Height})

            # create and write the transformation(resampling
            # and reprojection) info to the new tif file
            with rasterio.open(os.path.join(os.path.curdir,
                                            output_name + '.tif'),
                               'w', **kwargs) as dst:
                for i in range(1, src.count + 1):
                    reproject(source=rasterio.band(src, i),
                              destination=rasterio.band(dst, i),
                              src_transform=src.transform,
                              src_crs=src.crs,
                              dst_transform=Affine,
                              dst_crs=Output_CRS,
                              dst_nodata=0,
                              resampling=Resampling.bilinear)

        # render a plot with the new crs and coaser version
        with rasterio.open(os.path.join(os.path.curdir,
                                        output_name + '.tif')) as dataset_:
            fig, ax = plt.subplots(1, 1)
            plot.show_hist(dataset_.read(1),
                           bins=200,
                           histtype='stepfilled',
                           title=output_name,
                           ax=ax)
            # set xand y axis label
            ax.set_xlabel('Value')
            ax.set_ylabel('Frequency')

        # retuen raster to plot the PrettyTable
        summary = summary_dem(os.path.join(os.path.curdir,
                                           output_name + '.tif'))

    except FileNotFoundError:
        print("Wrong file or file path")

    return summary


def gaussianFilter(sizex, sizey=None, scale=0.333):
    '''
    Generate and return a 2D Gaussian function
    of dimensions (sizex,sizey)
    If sizey is not set, it defaults to sizex
    A scale can be defined to widen the function (default = 0.333)
    '''
    sizey = sizey or sizex
    x, y = np.mgrid[-sizex:sizex + 1, -sizey:sizey + 1]
    g = np.exp(-scale * (x**2 / float(sizex) + y**2 / float(sizey)))
    return g / g.sum()


def grad2d(dem, cell_size):
    '''
    Calculate the slope and gradient of a DEM
    '''
    f0 = gaussianFilter(3)
    # applies smooothing by gaussian filter
    i = signal.convolve(dem, f0, mode='valid')
    # SOBEL FILTER
    f1 = np.array([[0, 0, 0], [1, 0, -1], [0, 0, 0]])
    f2 = f1.transpose()
    g1 = signal.convolve(i, f1, mode='valid') / (cell_size * 2)
    g2 = signal.convolve(i, f2, mode='valid') / (cell_size * 2)
    slope = np.sqrt(g1**2 + g2**2)
    aspect = np.arctan(slope)
    return slope, aspect


# 2FD Algorithms
def slope_calculation_2FD(raster_path):
    with rasterio.open(raster_path, 'r+') as src:
        data = src.read()[0]
        # 2FD algorithms
        slope, aspect = grad2d(data, src.res[0])

        slope_degree = np.full(slope.shape, 1)
        for i in range(0, len(slope)):
            for j in range(0, len(slope[0])):
                # Convert value to slope (degree)
                slope_degree[i][j] = abs(math.atan(slope[i][j])
                                         * 180 / math.pi)

        result = np.array(slope_degree).flatten()
        return result


# Maximum Max Algorithms
def slope_calculation_MAX(raster_path):
    with rasterio.open(raster_path, 'r+') as src:
        data = src.read()[0]
        slope_degree = np.full(data.shape, 1)
        mf = math.sqrt(2) * src.res[0]
        cs = src.res[0]

        for i in range(1, data.shape[0] - 1):
            for j in range(1, data.shape[1] - 1):
                # maximum max value algorithms
                slope = max((data[i, j + 1] - data[i, j]) / cs,
                            (data[i + 1, j] - data[i, j]) / cs,
                            (data[i, j - 1] - data[i, j]) / cs,
                            (data[i - 1, j] - data[i, j]) / cs,
                            (data[i + 1, j + 1] - data[i, j]) / mf,
                            (data[i + 1, j - 1] - data[i, j]) / mf,
                            (data[i - 1, j + 1] - data[i, j]) / mf,
                            (data[i - 1, j - 1] - data[i, j]) / mf)

                # Convert value to slope (degree)
                slope_degree[i, j] = abs(math.atan(slope) * 180 / math.pi)

        result = np.array(slope_degree).flatten()
        return result


def main():
    dict_summary = summary_dem(os.path.join(os.path.curdir, "CLIP.tif"))
    display_summary(dict_summary)
    visualization(os.path.join(os.path.curdir, "CLIP.tif"))
    _2x_summary = dataset_conversion(0.5, "TVResampReproj")
    display_summary(_2x_summary)
    _4x_summary = dataset_conversion(0.25, "FVResampReproj")
    display_summary(_4x_summary)

    _2x_2fd = slope_calculation_2FD(os.path.join(os.path.curdir,
                                                 "TVResampReproj.tif"))
    _2x_max = slope_calculation_MAX(os.path.join(os.path.curdir,
                                                 "TVResampReproj.tif"))
    plt.hist(_2x_max, bins=60, rwidth=10,
             color="skyblue", alpha=0.8, label="max")
    plt.hist(_2x_2fd, bins=60, rwidth=10,
             color="pink", alpha=0.5, label="2fd")
    plt.ylim(0, 100000)
    plt.xlabel("Value", size=14)
    plt.ylabel("Frequency", size=14)
    plt.title("Multiple Histograms of 2X Slope Distribution with Matplotlib")
    plt.legend(loc='upper right')
    _4x_2fd = slope_calculation_2FD(os.path.join(os.path.curdir,
                                                 "FVResampReproj.tif"))
    _4x_max = slope_calculation_MAX(os.path.join(os.path.curdir,
                                                 "FVResampReproj.tif"))
    plt.hist(_4x_max, bins=60, rwidth=10,
             color="skyblue", alpha=0.8, label="max")
    plt.hist(_4x_2fd, bins=60, rwidth=10,
             color="pink", alpha=0.5, label="2fd")
    plt.ylim(0, 50000)
    plt.xlabel("Value", size=14)
    plt.ylabel("Frequency", size=14)
    plt.title("Multiple Histograms of 4X Slope Distribution with Matplotlib")
    plt.legend(loc='upper right')


if __name__ == '__main__':
    main()
