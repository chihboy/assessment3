#!/usr/bin/python3

import unittest
import os
import rasters


class TestTaskOne(unittest.TestCase):
    def setUp(self):
        self.filename = os.path.join(os.path.curdir,"CLIP.tif")
        self.summary_output = 2391287.389

    def test_import_raster(self):
        summary_format = rasters.summary_dem(self.filename)
        self.assertTrue(isinstance(summary_format, (list, dict, tuple)),
                        "Output not a list, tuple or dictionary. \
                        Ignore this error if you defined a class.")

    def test_process_dictionary(self):
        ''' Test summary_dem function'''
        summary_dictionary = rasters.summary_dem(self.filename)
        summary_dictionary = summary_dictionary['Min y, Min Lat'].split("m")[0]
        self.assertAlmostEqual(float(summary_dictionary), self.summary_output)


class TestTaskTwo(unittest.TestCase):
    def setUp(self):
        self.summary_output = 2539962.8357

    def test_ResamplingReporject_process_dictionary(self):
        ''' Test ResamplingReporject function'''
        summary_dictionary = rasters.dataset_conversion(0.5, "TVResampReproj_test")
        summary_dictionary = summary_dictionary['Min y, Min Lat'].split("m")[0]
        self.assertAlmostEqual(float(summary_dictionary), self.summary_output)




if __name__ == '__main__':
    unittest.main()
